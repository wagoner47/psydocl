#import distutils
#from distutils.core import setup, Extension, Command
#
#setup(
#    name="psydocl", 
#    packages=['psydocl'],
#    version="0.1",
#)
#
from distutils.core import setup
from Cython.Build import cythonize
from Cython.Distutils import Extension
from Cython.Distutils import build_ext
import numpy
#import cython_gsl

extensions = [Extension("psydocl.mixing_c",
                        ["psydocl/mixing_c.pyx"],
                         libraries=['wigxjpf'],
                         library_dirs=["/global/homes/m/maccrann/code/wigxjpf-1.6/lib"], #"/Users/maccrann.2/code/wigxjpf-1.6/lib"],
                         include_dirs=[numpy.get_include(),"/global/homes/m/maccrann/code/wigxjpf-1.6/inc"],
                         extra_compile_args=['-fopenmp'],
        				 extra_link_args=['-fopenmp'],)]
setup(
    name = "psydocl",
    packages=['psydocl'],
    version="0.1",
    #language = "c++",
    cmdclass = {'build_ext': build_ext},
    ext_modules = cythonize(extensions, annotate=True, force=True) #, language='c++')
)

