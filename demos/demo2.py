# Slow demo
#- Generate N gaussian random shear map from a simple power spectrum
#- Ensure unbiased signal after correcting for mask
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import psydocl
import numpy as np
import healpy as hp
import argparse
import os
from os.path import join as pj
import time
import pylab

def parse_args():
    import argparse
    description = 'Test pseudo-cl code'

    parser = argparse.ArgumentParser(description=description, add_help=True,
                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('outdir',type=str)
    parser.add_argument('--n_real','-n',type=int,default=10,help="number of realisations")
    parser.add_argument('--lmin',type=int,default=10,help="lmin")
    parser.add_argument('--lmax',type=int,default=30,help="lmax")
    parser.add_argument('--nbin',type=int,default=5)
    parser.add_argument('--nside',type=int,default=128)
    parser.add_argument('--mpi',action='store_true',default=False,help='run using mpi')
    args=parser.parse_args()
    return args
    
def main():
    args=parse_args()

    if args.mpi:
        from mpi4py import MPI
        from mpi4py.MPI import ANY_SOURCE
        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()
        size = comm.Get_size()
    else:
        rank=0
        comm=None

    print 'comm',comm

    #make output dir
    if rank==0:
        if not os.path.isdir(args.outdir):
            os.makedirs(args.outdir)

    #Generate a wiggly input Cl - function wigglyCl in cltools for testing
    ells, c_ell = psydocl.wigglyCl(0,5*args.lmax,wiggle_freq=0.001)
    Cl_dict = {"EE":c_ell}
    #choose an nside
    nside = args.nside

    #simple mask
    #theta,phi = hp.pix2ang(nside, np.arange(hp.nside2npix(nside)))
    #mask = np.ones_like(theta)
    #to_mask = (theta<0.) | (theta>np.pi/2) | (phi<0.) | (phi>np.pi/2)
    #mask[to_mask] = 0.
    mask = hp.read_map('mask_512.fits')
    mask[mask<0.]=0.

    lbinner = psydocl.ClBinner(lmin=args.lmin,lmax=args.lmax,num_ell_bins=args.nbin,pad_frac_hi=1.5)
    binned_input_cl = lbinner.bin_cl(ells, c_ell)
    if rank==0:
        print binned_input_cl

    #Now get mixing matrix
    mixer1 = psydocl.MixingMatrixFull(lbinner.lmin, lbinner.lmax, mask)
    mixer2 = psydocl.MixingMatrixFull(lbinner.lmin, lbinner.lmax, mask)
    mixer3 = psydocl.MixingMatrixFull(lbinner.lmin, lbinner.lmax, mask)
    #mix = mixer.get_mixing_mpi(comm, TT=False,TP=False)
    t0=time.time()
    mix1 = mixer1.get_mixing_single("PP")
    print 'python', time.time()-t0
    t0=time.time()
    mix2 = mixer2.get_mixing_single("PP",c=True)
    print 'c', time.time()-t0
    t0=time.time()
    mix3 = mixer3.get_mixing_single_clip("PP",clip=0.01)
    print 'c clipped', time.time()-t0

    print mix2['EE_EE']/mix3['EE_EE']
    
    if comm:
        comm.Barrier()

    if rank==0:
        M_binned=mixer1.bin_Ms(lbinner=lbinner, nside=args.nside)
        M_binned2=mixer2.bin_Ms(lbinner=lbinner, nside=args.nside)
        M_binned3=mixer3.bin_Ms(lbinner=lbinner, nside=args.nside)
        pylab.figure(1)
        pylab.imshow(M_binned['EE_EE'],origin='lower',interpolation='nearest')
        pylab.colorbar()
        pylab.savefig(pj(args.outdir,'mixing_matrix_binned.png'))

    #and generate a GaussianRealMap object.
    shear_mapper = psydocl.GaussianRealMap(nside, ells, Cl_dict, weight_map=mask)

    #Now generate a load of masked random fields, measure cls, 
    raw_cls = shear_mapper.realise_cls(comm=comm,num_real=args.n_real)

    #rank 0 can do the rest
    if rank==0:
        corrected_cls = []
        corrected_cls2 = []
        corrected_cls3 = []
        for cl in raw_cls:
            x = mixer1.binned_inversion(cl)
            print 'cl_python/cl_c',x['EE']/mixer2.binned_inversion(cl)['EE']
            print 'cl_python/cl_c_cip',x['EE']/mixer3.binned_inversion(cl)['EE']
            corrected_cls.append(x)
            
        sum_cl_diff=np.zeros_like(lbinner.ell_bin_mids)
        sum_cl_diff_sq=np.zeros_like(lbinner.ell_bin_mids)
        for c in corrected_cls:
            cl_diff_frac =  (c['EE']-binned_input_cl)/binned_input_cl
            #pylab.plot(lbinner.ell_bin_mids, cl_diff_frac, 'k-', alpha=0.1)
            sum_cl_diff+=cl_diff_frac
            sum_cl_diff_sq+=cl_diff_frac**2
        mean_cl_diff = sum_cl_diff/len(corrected_cls)
        sigma_cl_diff = np.sqrt(sum_cl_diff_sq/len(corrected_cls) - mean_cl_diff**2)/np.sqrt(len(corrected_cls))
        pylab.figure(2)
        pylab.errorbar(lbinner.ell_bin_mids, mean_cl_diff, yerr=sigma_cl_diff)
        pylab.plot(lbinner.ell_bin_mids, np.zeros_like(lbinner.ell_bin_mids), 'k--', alpha=0.5)
        #pylab.ylim([-0.03,0.03])
        pylab.savefig(pj(args.outdir,'cl_diff.png')) 
    return 0

if __name__=="__main__":
    main()
