# README #

Tools for density and shear fields in python, including measuring pseudo-Cls, and generating correlated gaussian (and transformed gaussian) random fields. Still very much under development. The worker code e.g. computing the mixing matrices (see mixing.py) has been well tested, but lots still to do on the API and documentation.

Check out the [wiki](https://bitbucket.org/niallm1/psydocl/wiki/Home)

* Contact Niall MacCrann for more info


On my Mac (10.11.6) I had to do:
export CFLAGS="-I/usr/local/lib/python2.7/site-packages/numpy/core/include/ -I/Users/maccrann.2/code/wigxjpf-1.6/inc"
CC=/usr/local/Cellar/gcc/6.3.0_1/bin/gcc-6 
before doing python setup.py install. The second one is because the version of clang (the default C compiler) doesn't support openmp.