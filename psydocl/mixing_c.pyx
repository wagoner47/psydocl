import numpy as np
cimport numpy as np
cimport cython
from wig cimport wig_table_init, wig_temp_init, wig3jj, wig_table_free, wig_temp_free, wig_thread_temp_init
from cython.parallel import prange, parallel


DTYPE_INT = np.int
DTYPE_FLOAT = np.float64
ctypedef np.int_t DTYPE_INT_t
ctypedef np.float_t DTYPE_FLOAT_t



@cython.boundscheck(False)
@cython.wraparound(False)
def get_mixing_PP_wigxjpf_clip(int lmin, int lmax, np.ndarray[DTYPE_FLOAT_t, ndim=1] curvy_k, double clip):

    cdef np.ndarray ls = np.arange(lmin,lmax+1, dtype=DTYPE_INT)
    cdef int num_l=lmax+1-lmin

    wig_table_init(2*lmax, 3)
    with nogil, parallel():
        wig_thread_temp_init(2*lmax)

    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] M_EE_EE = np.zeros((num_l,num_l))
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] M_EE_BB = np.zeros((num_l,num_l))
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] M_EB_EB = np.zeros((num_l,num_l))
    
    cdef int i, j, li, lj, lk, j_dummy

    cdef double EE_EE_lk_sum, EE_BB_lk_sum, EB_EB_lk_sum,
    cdef double m_ee_ee_ieqj, m_ee_bb_ieqj, m_eb_eb_ieqj
    cdef double m_ee_ee, m_ee_bb, m_eb_eb
    cdef double prefactor, pol_wigner, add_to_sum
    cdef double PI = 3.14159265359

    for i in range(num_l):
        li = lmin+i
        #first compute li=lj entry
        lj = li
        prefactor= (2*lj+1.)/8./PI
        EE_EE_lk_sum=0.
        EE_BB_lk_sum=0.
        EB_EB_lk_sum=0.
        with nogil, parallel():
            for lk in prange(lmax+1, schedule='dynamic'):
                pol_wigner = wig3jj(2*li, 2*lj, 2*lk, 4, -4, 0)
                add_to_sum=(2*lk + 1) * curvy_k[lk] * pol_wigner**2
                EE_EE_lk_sum+= (1 + (-1)**(li+lj+lk)) * add_to_sum
                EE_BB_lk_sum+= (1 - (-1)**(li+lj+lk)) * add_to_sum
                EB_EB_lk_sum+= add_to_sum    

        m_ee_ee_ieqj = EE_EE_lk_sum * prefactor
        m_ee_bb_ieqj = EE_BB_lk_sum * prefactor
        m_eb_eb_ieqj = EB_EB_lk_sum * 2 * prefactor
        M_EE_EE[i,i] = m_ee_ee_ieqj
        M_EE_BB[i,i] = m_ee_bb_ieqj
        M_EB_EB[i,i] = m_eb_eb_ieqj

        #now lj<li, stopping when clip condition is satisfied
        for j_dummy in range(i):
            j = i - 1 - j_dummy
            lj =  lmin+j
            prefactor= (2*lj+1.)/8./PI
            EE_EE_lk_sum=0.
            EE_BB_lk_sum=0.
            EB_EB_lk_sum=0.
            with nogil, parallel():
                for lk in prange(lmax+1, schedule='dynamic'):
                    pol_wigner = wig3jj(2*li, 2*lj, 2*lk, 4, -4, 0)
                    add_to_sum=(2*lk + 1) * curvy_k[lk] * pol_wigner**2
                    EE_EE_lk_sum+= (1 + (-1)**(li+lj+lk)) * add_to_sum
                    EE_BB_lk_sum+= (1 - (-1)**(li+lj+lk)) * add_to_sum
                    EB_EB_lk_sum+= add_to_sum    
            m_ee_ee = prefactor*EE_EE_lk_sum
            m_ee_bb = prefactor*EE_BB_lk_sum
            m_eb_eb = 2*prefactor*EB_EB_lk_sum
            M_EE_EE[i,j] = m_ee_ee
            M_EE_BB[i,j] = m_ee_bb
            M_EB_EB[i,j] = m_eb_eb
            if ((m_ee_ee/m_ee_ee_ieqj < clip) and (m_ee_bb/m_ee_bb_ieqj < clip) and (m_eb_eb/m_eb_eb_ieqj < clip)):
                break
        #now lj>li, stopping when clip condition is satisfied
        for j_dummy in range(num_l-i-1):
            j = i + 1 + j_dummy
            lj =  lmin+j
            prefactor= (2*lj+1.)/8./PI
            EE_EE_lk_sum=0.
            EE_BB_lk_sum=0.
            EB_EB_lk_sum=0.
            with nogil, parallel():
                for lk in prange(lmax+1, schedule='dynamic'):
                    pol_wigner = wig3jj(2*li, 2*lj, 2*lk, 4, -4, 0)
                    add_to_sum=(2*lk + 1) * curvy_k[lk] * pol_wigner**2
                    EE_EE_lk_sum+= (1 + (-1)**(li+lj+lk)) * add_to_sum
                    EE_BB_lk_sum+= (1 - (-1)**(li+lj+lk)) * add_to_sum
                    EB_EB_lk_sum+= add_to_sum    
            m_ee_ee = prefactor*EE_EE_lk_sum
            m_ee_bb = prefactor*EE_BB_lk_sum
            m_eb_eb = 2*prefactor*EB_EB_lk_sum
            M_EE_EE[i,j] = m_ee_ee
            M_EE_BB[i,j] = m_ee_bb
            M_EB_EB[i,j] = m_eb_eb
            if ((m_ee_ee/m_ee_ee_ieqj < clip) and (m_ee_bb/m_ee_bb_ieqj < clip) and (m_eb_eb/m_eb_eb_ieqj < clip)):
                break


    wig_table_free()
    with nogil, parallel():
        wig_temp_free()

    return M_EE_EE, M_EE_BB, M_EB_EB

@cython.boundscheck(False)
@cython.wraparound(False)
def get_mixing_PP_wigxjpf(int lmin, int lmax, np.ndarray[DTYPE_FLOAT_t, ndim=1] curvy_k):

    cdef np.ndarray ls = np.arange(lmin,lmax+1, dtype=DTYPE_INT)
    #cdef np.ndarray lks = np.arange(lmax+1, dtype=DTYPE_INT)
    cdef int num_l=lmax+1-lmin

    wig_table_init(2*lmax, 3)
    with nogil, parallel():
        wig_thread_temp_init(2*lmax)

    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] M_EE_EE = np.zeros((num_l,num_l))
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] M_EE_BB = np.zeros((num_l,num_l))
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] M_EB_EB = np.zeros((num_l,num_l))
    
    cdef int i, j, li, lj, lk

    cdef double EE_EE_lk_sum, EE_BB_lk_sum, EB_EB_lk_sum,

    cdef double prefactor, pol_wigner, add_to_sum
    cdef double PI = 3.14159265359
    #probably need to make this a seperate function for parallelisation to work!!!
    #for i in prange(num_l, schedule='dynamic'):
    for i in range(num_l):
        li = lmin+i
        for j in range(num_l):
            lj =  lmin+j
            prefactor= (2*lj+1.)/8./PI
            EE_EE_lk_sum=0.
            EE_BB_lk_sum=0.
            EB_EB_lk_sum=0.
            with nogil, parallel():
                for lk in prange(lmax+1, schedule='dynamic'):
                    pol_wigner = wig3jj(2*li, 2*lj, 2*lk, 4, -4, 0)
                    add_to_sum=(2*lk + 1) * curvy_k[lk] * pol_wigner**2
                    EE_EE_lk_sum+= (1 + (-1)**(li+lj+lk)) * add_to_sum
                    EE_BB_lk_sum+= (1 - (-1)**(li+lj+lk)) * add_to_sum
                    EB_EB_lk_sum+= add_to_sum    

            M_EE_EE[i,j] = prefactor*EE_EE_lk_sum
            M_EE_BB[i,j] = prefactor*EE_BB_lk_sum
            M_EB_EB[i,j] = 2*prefactor*EB_EB_lk_sum

    wig_table_free()
    with nogil, parallel():
        wig_temp_free()

    return M_EE_EE, M_EE_BB, M_EB_EB

@cython.boundscheck(False)
def get_mixing_PP_wigxjpf_singlethread(int lmin, int lmax, np.ndarray[DTYPE_FLOAT_t, ndim=1] curvy_k):

    cdef np.ndarray ls = np.arange(lmin,lmax+1, dtype=DTYPE_INT)
    #cdef np.ndarray lks = np.arange(lmax+1, dtype=DTYPE_INT)
    cdef int num_l=lmax+1-lmin

    wig_table_init(2*lmax, 3)
    with nogil, parallel():
        wig_thread_temp_init(2*lmax)
    wig_temp_init(2*lmax)

    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] M_EE_EE = np.zeros((num_l,num_l))
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] M_EE_BB = np.zeros((num_l,num_l))
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] M_EB_EB = np.zeros((num_l,num_l))
    
    cdef int i, j, li, lj, lk

    cdef double EE_EE_lk_sum, EE_BB_lk_sum, EB_EB_lk_sum,

    cdef double prefactor, pol_wigner, add_to_sum
    cdef double PI = 3.14159265359
    #probably need to make this a seperate function for parallelisation to work!!!
    #for i in prange(num_l, schedule='dynamic'):
    for i in range(num_l):
        li = lmin+i
        for j in range(num_l):
            lj =  lmin+j
            prefactor= (2*lj+1.)/8./PI
            EE_EE_lk_sum=0.
            EE_BB_lk_sum=0.
            EB_EB_lk_sum=0.
            for lk in range(lmax+1):
                pol_wigner = wig3jj(2*li, 2*lj, 2*lk, 4, -4, 0)
                add_to_sum=(2*lk + 1) * curvy_k[lk] * pol_wigner**2
                EE_EE_lk_sum+= (1 + (-1)**(li+lj+lk)) * add_to_sum
                EE_BB_lk_sum+= (1 - (-1)**(li+lj+lk)) * add_to_sum
                EB_EB_lk_sum+= add_to_sum    

            M_EE_EE[i,j] = prefactor*EE_EE_lk_sum
            M_EE_BB[i,j] = prefactor*EE_BB_lk_sum
            M_EB_EB[i,j] = 2*prefactor*EB_EB_lk_sum

    wig_table_free()
    wig_temp_free()

    return M_EE_EE, M_EE_BB, M_EB_EB

@cython.boundscheck(False)
@cython.wraparound(False)
def get_mixing_TT_wigxjpf(int lmin, int lmax, np.ndarray[DTYPE_FLOAT_t, ndim=1] curvy_k):

    cdef np.ndarray ls = np.arange(lmin,lmax+1, dtype=DTYPE_INT)
    #cdef np.ndarray lks = np.arange(lmax+1, dtype=DTYPE_INT)
    cdef int num_l=lmax+1-lmin

    wig_table_init(2*lmax, 3)
    with nogil, parallel():
        wig_thread_temp_init(2*lmax)

    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] M_TT_TT = np.zeros((num_l,num_l))
    
    cdef int i, j, li, lj, lk

    cdef double TT_TT_lk_sum

    cdef double prefactor, temp_wigner, add_to_sum
    cdef double PI = 3.14159265359
    for i in range(num_l):
        li = lmin+i
        for j in range(num_l):
            lj =  lmin+j
            prefactor= (2*lj+1.)/8./PI
            TT_TT_lk_sum=0.
            with nogil, parallel():
                for lk in prange(lmax+1, schedule='dynamic'):
                    temp_wigner = wig3jj(2*li, 2*lj, 2*lk, 0, 0, 0)
                    TT_TT_lk_sum += (2*lk + 1) * curvy_k[lk] * temp_wigner**2

            M_TT_TT[i,j] = 2*prefactor*TT_TT_lk_sum

    wig_table_free()
    with nogil, parallel():
        wig_temp_free()

    return M_TT_TT

@cython.boundscheck(False)
@cython.wraparound(False)
def get_mixing_TP_wigxjpf(int lmin, int lmax, np.ndarray[DTYPE_FLOAT_t, ndim=1] curvy_k):

    cdef np.ndarray ls = np.arange(lmin,lmax+1, dtype=DTYPE_INT)
    #cdef np.ndarray lks = np.arange(lmax+1, dtype=DTYPE_INT)
    cdef int num_l=lmax+1-lmin

    wig_table_init(2*lmax, 3)
    with nogil, parallel():
        wig_thread_temp_init(2*lmax)

    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] M_TE_TE = np.zeros((num_l,num_l))
    
    cdef int i, j, li, lj, lk

    cdef double TE_TE_lk_sum

    cdef double prefactor, temp_wigner, pol_wigner, add_to_sum
    cdef double PI = 3.14159265359
    for i in range(num_l):
        li = lmin+i
        for j in range(num_l):
            lj =  lmin+j
            prefactor= (2*lj+1.)/8./PI
            TE_TE_lk_sum=0.
            with nogil, parallel():
                for lk in prange(lmax+1, schedule='dynamic'):
                    temp_wigner = wig3jj(2*li, 2*lj, 2*lk, 0, 0, 0)
                    pol_wigner = wig3jj(2*li, 2*lj, 2*lk, 4, -4, 0)

                    TE_TE_lk_sum+= (2*lk + 1) * curvy_k[lk] * temp_wigner * pol_wigner 

            M_TE_TE[i,j] = 2*prefactor*TE_TE_lk_sum

    wig_table_free()
    with nogil, parallel():
        wig_temp_free()

    return M_TE_TE