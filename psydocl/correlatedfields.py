import numpy as np
from . import shear_mapper
import healpy as hp

def random_state(rank):
    seed=np.random.randint(0, 4294967295)+rank
    return np.random.RandomState(seed)

def shear_shear_cls(num_shear, ells, Cls_shear_shear, nside, lmax_est=None, masks=None, noise_maps=None, zero_ells=[0], 
                    n_real=5, comm=None, arr_cols=['EE'], verbosity=1):
    """Generate tomographic shear cls from correlated gaussian random fields, optionally with added noise
    Only works MPI right now...
    """

    if comm is not None:
        from mpi4py import MPI
        from mpi4py.MPI import ANY_SOURCE
        rank=comm.Get_rank()
        size=comm.Get_size()

        #lmax_est is the lmax you want to measure cl up to...if None, use maximum l of the input Cls
        if lmax_est is None:
            lmax_est=ells[-1]
        num_combs = num_shear * (num_shear+1) / 2

        if rank==0:
            #rank 0 just sets up a list of cl_dicts (realised cls) and collects from the other processes
            master=True
            cl_dicts=[]
            num_rec=0
            while num_rec<n_real:
                cl_dicts.append(comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG))
                num_rec+=1
                if verbosity>0:
                    if num_rec%10==0:
                        print('rank 0 recieved %d/%d gaussian cls'%(num_rec,n_real))
            return cl_dicts

        else:
            #Get a random state with seed dependent on rank (otherwise all ranks will have same random seed)
            rs=random_state(rank)
            #Initialise CorrelatedFields object, just with shear-shear arguments
            corrfields = CorrelatedFields(0, num_shear, ells, None, None, Cls_shear_shear, zero_ells=zero_ells)
            for i in range(n_real):
                if i%(size-1)!=rank-1:
                    continue
                #Generate list of shear maps:
                _,shear_maps=corrfields.get_shear_maps(nside=nside, rs=rs)
                if noise_maps is not None:
                    #if noise maps provided, also generate noise maps and add to shear maps
                    noisy_shear_maps=[]
                    for shear_map,noise_map in zip(shear_maps,noise_maps):
                        noise_real=noise_map.realise(rs=rs)
                        shear_map.map1+=noise_real.map1
                        shear_map.map2+=noise_real.map2
                        noisy_shear_maps.append(shear_map)
                    shear_maps = noisy_shear_maps
                if masks is not None:
                    #if mask provided, mask the shear maps
                    try:
                        len(masks)
                    except TypeError:
                        masks=[masks]*num_shear

                    masked_maps=[]
                    for shear_map,mask in zip(shear_maps,masks):
                        shear_map.map1[mask==0]=hp.UNSEEN
                        shear_map.map2[mask==0]=hp.UNSEEN
                        masked_maps.append(shear_map)

                    shear_maps = masked_maps

                #now loop through combinations of the shear maps, computing the cls, and returning a dictionary
                #of the cls to master. Each dicionary element will be a dictionary of cls (TT,TE etc.). The cls 
                #involving T will be zeros in this case
                cl_dict={}
                for i in range(num_shear):
                    for j in range(i,num_shear):
                        cl_dict[(i,j)] = shear_mapper.maps2cldict(shear_maps[i], lmax_est, map_2=shear_maps[j])
                comm.send(cl_dict, dest=0)
            return 0       

class CorrelatedFields(object):
    """Class for generating correlated gaussian random fields
    num_pos is number of scalar fields, num_shear is number of spin-2 fields
    Cls_pos_pos, Cls_pos_shear, Cls_shear_shear are dictionaries containing the
    power spectra. Cls_X_Y has keys (i,j), where i and j denote the ith X-field
    and jth Y-field. "ells" is the l values of the Cls_X_Y.
    zero_ells will be set to have zero amplitude (cholesky decomposition skipped)"""

    def __init__(self, num_pos, num_shear, ells, Cls_pos_pos, Cls_pos_shear, Cls_shear_shear, zero_ells=[0], shear_pos=False):

        self.ells=ells
        assert ells[0]==0
        self.lmax=self.ells[-1]
        self.num_pos=num_pos
        self.num_shear=num_shear
        self.n_fields=num_pos + num_shear
        self.field_inds=range(self.n_fields)
        self.field_types=[]
        for i in range(num_pos):
            self.field_types.append('pos')
        for i in range(num_shear):
            self.field_types.append('shear')
        print(self.field_types)

        self.Cls_pos_pos,self.Cls_shear_shear = Cls_pos_pos, Cls_shear_shear
        if shear_pos:
            self.Cls_pos_shear={}
            for key,val in Cls_pos_shear.iteritems():
                self.Cls_pos_shear[(key[1],key[0])]=val
        else:
            self.Cls_pos_shear=Cls_pos_shear

        #print "shear_shear",self.Cls_shear_shear
        #print "pos_shear",self.Cls_pos_shear
        #print "pos_pos",self.Cls_pos_pos


        self.zero_ells=zero_ells
        self.get_amplitude_matrices()


    def get_shear_maps(self, nside, rs=None, lognormalize=None, seed=None):
        print('getting shear maps')
        if seed is not None:
            np.random.seed(seed)
        if self.n_fields==1:
            if self.field_types[0]=='shear':
                g=shear_mapper.GaussianRealMap(nside, self.ells, {'TT':self.Cls_shear_shear[0,0],'EE':self.Cls_shear_shear[0,0]})
                ms_raw=g.realise_map(ret_raw_maps=True)
                return [],[shear_mapper.ShearMap(nside, ms_raw[1],ms_raw[2],kappa_map=ms_raw[0])]

            if self.field_types[0]=='pos':
                g=shear_mapper.GaussianRealMap(nside, self.ells, {'TT':self.Cls_pos_pos[0,0]})
                return [g.realise_map(T_only=True)],[]

        if rs is None:
            rs=np.random.RandomState()
        alm_obj=hp.Alm()
        ls_alm,ms_alm=alm_obj.getlm(self.lmax)
        size_alm=len(ls_alm)
        #Loop through fields, generating random alms with variance 1
        field_alms_real=np.zeros((self.n_fields, len(ls_alm)))
        field_alms_imag=np.zeros((self.n_fields, len(ls_alm)))

        for field_i in range(self.n_fields):
            field_alms_real[field_i] = rs.normal(size=size_alm)
            field_alms_imag[field_i] = rs.normal(size=size_alm)
        field_alms=field_alms_real + field_alms_imag*1j
        #Now scale by amplitude matrices, since ls start from zero,
        #l gives the index of the amplitude matrix required for each
        #alm.

        for i,l_alm in enumerate(ls_alm):
            scaled_alms = np.dot(self.amp_chols[l_alm], field_alms[:,i]) / np.sqrt(2)
            field_alms[:,i] = scaled_alms

        #Now we have the alms, just need to generate the fields
        pos_fields,shear_fields=[],[]
        for field_i in range(self.n_fields):
            if self.field_types[field_i]=='pos':
                alms=(field_alms[field_i])
                map_out=hp.alm2map(alms, nside, pol=False)
                pos_fields.append(shear_mapper.Map(nside, map_out))
            else:
                alms=(field_alms[field_i],field_alms[field_i],np.zeros_like(field_alms[field_i]))
                maps_out=hp.alm2map(alms, nside, pol=True)
                shear_fields.append(shear_mapper.ShearMap(nside, maps_out[1], maps_out[2], kappa_map=maps_out[0]))
        
        return pos_fields,shear_fields

    def get_amplitude_matrices(self):
        """This function gets the amplitude matrices, L for each l. These are calculated as follows -
        the matrix C_ij is constructed, where C_ij = Cl_fieldi_fieldj. Then L L^T = C_ij."""
    
        Cl_covs=np.zeros((len(self.ells),self.n_fields,self.n_fields))
        for i in range(self.n_fields):
            for j in range(i,self.n_fields):
                if self.field_types[i]=='pos':
                    if self.field_types[j]=='pos':
                        Cl=self.Cls_pos_pos[(i,j)]
                    if self.field_types[j]=='shear':
                        Cl=self.Cls_pos_shear[(i,j-self.num_pos)]
                if self.field_types[i]=='shear':
                    Cl=self.Cls_shear_shear[(i-self.num_pos,j-self.num_pos)]
                Cl_covs[:,i,j]=Cl
                Cl_covs[:,j,i]=Cl

        self.amp_chols=np.zeros_like(Cl_covs)
        for i in self.ells:
            if i in self.zero_ells:
                continue
            try:
                self.amp_chols[i] = np.linalg.cholesky(Cl_covs[i])
            except np.linalg.linalg.LinAlgError:
                print("Cholesky failed for l = %d",i)
                cov=Cl_covs[i]
                print("cov:",cov)
                print("det(cov):",np.linalg.det(cov))
                cl_corr=np.zeros_like(cov)
                for p in range(len(cl_corr)):
                    for q in range(len(cl_corr)):
                        cl_corr[p,q]=cov[p,q]/np.sqrt(cov[p,p]*cov[q,q])
                print("corr",cl_corr)
