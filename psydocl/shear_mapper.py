import fitsio
import healpy as hp
import scipy.interpolate as interp
import numpy as np
import healpix_util as hu
import sys

#Default dictionary of column names...these are the BCC ones
shape_col_dict={'e1':'EPSILON1',
                'e2':'EPSILON2',
                'g1':'GAMMA1',
                'g2':'GAMMA2',
                'dec':'DEC',
                'ra':'RA',
                'w':None}

def random_seed(rank):
    return np.random.randint(4294967295)+rank

def random_state(rank):
    seed=random_seed(rank)
    return np.random.RandomState(seed)

class Maps(object):
    """Class for storing/saving a list of maps"""
    def __init__(self, maps, scheme="ring"):
        self.maps = maps
        self.nside = self.maps[0].nside
        for m in self.maps[1:]:
            assert m.nside==self.nside

class Map(object):
    def __init__(self, nside, map1, map2=None,scheme="ring",weight_map=None, apply_weight_map=False):
        self.nside=nside
        self.npix=hp.nside2npix(self.nside)
        self.pixinds=np.arange(self.npix)
        assert len(map1)==self.npix
        self.map1=map1
        self.map1[np.isnan(map1)]=hp.UNSEEN
        self.spin=0
        self.scheme=scheme
        self.hpix=hu.HealPix(scheme,self.nside)
        if map2 is not None:
            assert len(map2)==self.npix
            self.map2=map2
            self.map2[np.isnan(map2)]=hp.UNSEEN
            self.spin=2
        if weight_map is not None:
            if apply_weight_map:
                self.apply_weight(weight_map)
            else:
                self.weight_map=weight_map
        self.unseen = (self.map1==hp.UNSEEN)

    def get_rho(self):
        non_zero_pix = self.weight_map>0.
        rho1 = np.mean(self.map1[non_zero_pix]/(self.weight_map/self.weight_map.max())[non_zero_pix])
        if self.spin==2:
            rho2 = np.mean(self.map2[non_zero_pix]/(self.weight_map/self.weight_map.max())[non_zero_pix])
            return rho1,rho2
        else:
            return rho1

    def set_mask(self, mask):
        assert len(self.map1)==len(mask)
        to_mask=(mask==0)
        self.map1[to_mask]=hp.UNSEEN
        if self.spin==2:
            self.map2[to_mask]=hp.UNSEEN
        if self.weight_map:
            self.weight_map[to_mask]=0.
        else:
            self.weight_map=mask

    def write(self, filename):
        """Use healpy for now...it expects either 1 map or 3 (I,Q,U) maps
        if e.g. a pair of shear maps, put in a dummy map of zeros as first
        map"""
        try:
            to_save=(np.zeros_like(self.map1),self.map1,self.map2)
        except AttributeError:
            to_save=self.map1
        hp.write_map(filename, to_save)

    def apply_weight(self, weight_map):
        self.map1 *= weight_map
        if self.spin==2:
            self.map2 *= weight_map
        self.set_weight_map(weight_map)

    def set_weight_map(self, weight_map):
        self.weight_map = weight_map

    def get_healpy_input(self,pol=False):
        if self.spin==0:
            if not pol:
                self.healpy_input = self.map1
            else:
                self.healpy_input = (self.map1, np.zeros_like(self.map1), np.zeros_like(self.map1))
        else:
            self.healpy_input = (np.zeros_like(self.map1), self.map1, self.map2)
        return self.healpy_input

    def flip(self):
        self.map1[~self.unseen] *= -1
        if self.spin==2:
            self.map2[~self.unseen] *= -1

    def flip2(self):
        self.map2[~self.unseen] *= -1

    def flip1(self):
        self.map1[~self.unseen] *= -1

def overdensity_from_raw(raw_map, nside, weight_map=None, rho=None):
    """make Map object of overdensity"""
    if rho is None:
        try:
            len(weight_map)
            non_zero_weight=weight_map>0.
            rho = np.mean(raw_map[non_zero_weight] / (weight_map/weight_map.max())[non_zero_weight])
        except TypeError as e:
            print("you need to either provide rho or weight_map")
            raise e
    print("converting map to overdensity with rho",rho)
    overdensity_map = (raw_map - rho)/rho
    return Map(nside, overdensity_map, weight_map=weight_map)

class OverDensityMap(Map):
    """map1 should be a map of overdensity e.g. galaxy overdensity would be b_g * delta
    for linear bias"""
    def lognormalize(self, k0=0.05, inplace=True):
        var_od=np.var(self.map1)
        shifted_od = self.map1 - 0.5*var_od/k0
        lognorm_od = k0 * (np.exp(shifted_od/k0) -1)
        if inplace:
            self.map1=lognorm_od
        else:
            return Map(self.nside, lognorm_od)

    def get_number_denisty_map(self, random_exp_map, rs=None, get_cat=False):
        """random_exp_map is the expected number of galaxies in each pixel"""
        """We want to modulate this by the bias x over-density, then poisson sample"""
        """each pixel with the resulting mean"""
        if rs==None:
            rs=np.random.RandomState()
        number_density_mean = random_exp_map * (1 + self.map1)
        non_zero_inds=(number_density_mean>0)
        number_density_real=np.zeros_like(len(number_density_mean))
        number_density_real[non_zero_inds] = rs.poisson(lam=number_density_mean[non_zero_inds], size=non_zero_inds.sum())
        if get_cat:
            hpix=hu.HealPix("ring",self.nside)
            npix=hpix.npix
            ra,dec=hpix.pix2eq(npix)

        return Map(self.nside, number_density_real)

class ShearMap(Map):
    def __init__(self, nside, map1, map2, kappa_map=None):
        super(ShearMap, self).__init__(nside, map1, map2=map2)
        self.kappa_map=kappa_map


    def get_shear_cat(self, N_gal, mask=None, ra_range=None, dec_range=None):
        if mask is None:
            mask=np.copy(self.mask)
        mask[mask<0.]=0.
        dmap=hu.DensityMap("ring",mask)
        ra,dec = genrand(N_gal, ra_range=ra_range, dec_range=dec_range)
        return self.interpolate_ra_dec(ra,dec)

    def interpolate_ra_dec(self,ra,dec):
        hpix=hu.HealPix("ring",self.nside)
        pixnums = hpix.eq2pix(ra,dec)
        theta,phi=hpix.pix2ang(pixnums)
        return self.interpolate_theta_phi(theta,phi)

    def interpolate_theta_phi(self,theta,phi):
        return hp.get_interp_val(self.map1, theta, phi), hp.get_interp_val(self.map2, theta, phi)

    def lognormalize(self,k0=0.05,mask=None):
        """Return new ShearMap that is a lognormal version of the original"""        
        if self.kappa_map is None:
            raise ValueError("Need to a kappa map to get lognormal shear field")
        var_kappa=np.var(self.kappa_map)
        sigma_kappa=np.sqrt(var_kappa)
        shifted_kappa = self.kappa_map - 0.5*var_kappa/k0
        lognorm_kappa = k0 * (np.exp(shifted_kappa/k0) -1)
        #Now we have lognormal kappa field, need to convert to alms, and re-generate shear fields
        kappa_alms = hp.map2alm(lognorm_kappa,pol=False)
        alms = (kappa_alms, kappa_alms, np.zeros_like(kappa_alms))
        maps = hp.alm2map(alms, nside=self.nside, pixwin=True)
        return ShearMap(self.nside, maps[1], maps[2], kappa_map=lognorm_kappa)        

def healpy_list_to_dict(cls_list,pol=True):
    """healpy returns list of cls TT,EE,BB,TE,EB,TB"""
    """turn this into a dict, so don't have to keep track of this order"""
    if not pol:
        return {"TT":cls_list}
    names=["TT","EE","BB","TE","EB","TB"]
    cls_dict={}
    for cl,name in zip(cls_list, names):
        cls_dict[name]=cl
    return cls_dict

def cl_dict_sub_noise(cl_dict, noise_dict):
    cl_sub={}
    for key,val in cl_dict.iteritems():
        cl_sub[key]=val-noise_dict[key]
    return cl_sub

class NoiseMap(Map):
    """Map which actually has noise sigma as the pixel values - can be used
    to generate noise realisations to subtract from power spectrum estimate"""

    def __init__(self, *args, **kwargs):
        try:
            self.noise_type=kwargs.pop('noise_type')
        except KeyError:
            self.noise_type='Gaussian'
        try:
            self.raw_count=kwargs.pop('raw_count')
        except KeyError:
            self.raw_count=False
        try:
            self.rho=kwargs.pop('rho')
        except KeyError:
            self.rho=None
        super(NoiseMap,self).__init__(*args, **kwargs)
        if self.raw_count:
            assert (self.rho or hasattr(self, "weight_map"))
        self.set_good_pix()

    def set_good_pix(self):
        if self.noise_type=='Gaussian':
            self.good_pix = (self.map1>0)
            if self.spin==2:
                self.good_pix *= (self.map2>0)
        elif self.noise_type=="Poisson":
            self.good_pix = (self.map1>=0)
            if self.spin==2:
                self.good_pix *= (self.map2>=0)
        elif self.noise_type=="RotateShear":
            assert self.spin==2
            self.good_pix = self.map1!=hp.UNSEEN
            self.good_pix *= self.map2!=hp.UNSEEN
        else:
            raise ValueError("noise_type should be one of 'Gaussian', 'Poisson', 'RotateShear'")
        self.num_good_pix = self.good_pix.sum()

    def set_mask(self, mask):
        super(NoiseMap, self).set_mask(mask)
        self.set_good_pix()

    def get_noise_cls(self, lmax, num_real, comm=None):
        master=False
        if comm is None:
            master=True
            cls=[]
            #generate num_real realisations of the noise, measure cls, and return mean
            for i_real in range(num_real):
                
                shear_map=self.realise()
                if self.spin==0:
                    maps=shear_map.map1
                else:
                    maps=(np.zeros_like(shear_map.map1), shear_map.map1, shear_map.map2)
                cl=hp.anafast(maps, lmax=lmax)
                cls.append(cl)
            cls=np.array(cls)
            cls_mean=np.mean(cls, axis=0)
            cls_var=np.var(cls, axis=0)
        else:
            from mpi4py import MPI
            from mpi4py.MPI import ANY_SOURCE
            rank=comm.Get_rank()
            size=comm.Get_size()

            if rank==0:
                master=True
                cls_array=[]
                num_rec=0
                while num_rec < num_real:
                    cls = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG)
                    cls_array.append(cls)
                    num_rec+=1
                cls_array=np.array(cls_array)
                #print 'cls_array', cls_array
                #print cls_array.shape
                cls_mean=np.mean(cls_array, axis=0)
                cls_var=np.var(cls_array, axis=0)
                #print cls_mean.shape

            else:
                rs=random_state(rank)
                rs2=random_state(rank+9999)
                for i in range(num_real):
                    if i%(size-1)!=rank-1:
                        continue
                    shear_map=self.realise(rs=rs,rs2=rs2, convert_to_od=False)
                    if self.spin==0:
                        teb_maps=shear_map.map1
                    else:
                        teb_maps=(np.zeros_like(shear_map.map1), shear_map.map1, shear_map.map2)
                    cl=hp.anafast(teb_maps, lmax=lmax)
                    comm.send(cl, dest=0)
                return 0

        if master:
            return healpy_list_to_dict(cls_mean, pol=(self.spin==2)),healpy_list_to_dict(cls_var, pol=(self.spin==2))

    def realise(self, rs=None, rs2=None, convert_to_od=False, unseen=True):
        #if unseen=True, return pixels with zero noise as hp.UNSEEN. Else return them as zeros.
        if self.spin==0:
            rand_map=self.realise_map(self.map1, rs=rs, unseen=unseen)
            if self.raw_count:
                if self.rho:
                    return overdensity_from_raw(rand_map, self.nside, rho=self.rho)
                else:
                    return overdensity_from_raw(rand_map, self.weight_map, self.nside)
            else:
                return Map(self.nside, rand_map)
        else:
            if self.noise_type=='RotateShear':
                rand_map1, rand_map2 = self.realise_RandomShear(rs=rs, unseen=unseen)
            else:
                rand_map1, rand_map2 = self.realise_map(self.map1, rs=rs, unseen=unseen), self.realise_map(self.map2, rs=rs2, unseen=unseen)
            return Map(self.nside, rand_map1, map2=rand_map2)

    def realise_RandomShear(self, rs=None, unseen=True):
        if unseen:
            m1=np.ones_like(self.map1)*hp.UNSEEN
            m2=np.ones_like(self.map2)*hp.UNSEEN
        else:
            m1=np.zeros_like(self.map1)
            m2=np.zeros_like(self.map2)
        if rs is None:
            rs=np.random.RandomState()

        rand_phis = rs.rand(self.good_pix.sum())*2*np.pi
        e_total=np.sqrt(self.map1[self.good_pix]**2+self.map2[self.good_pix]**2)
        m1[self.good_pix] = e_total*np.cos(2*rand_phis)
        m2[self.good_pix] = e_total*np.sin(2*rand_phis)
        return m1,m2

    def realise_map(self,mapi,rs=None, unseen=True):
        if rs is None:
            rs=np.random.RandomState()
        if unseen:
            random_map=np.ones_like(mapi)*hp.UNSEEN
        else:
            random_map=np.zeros_like(mapi)
        if self.noise_type == 'Gaussian':
            random_map[self.good_pix] = mapi[self.good_pix] * rs.normal(size=self.num_good_pix)
        elif self.noise_type == 'Poisson':
            random_map[self.good_pix] = rs.poisson(lam=mapi[self.good_pix], size=self.good_pix.sum())
        return random_map


class GaussianRealMap(object):
    """Code to generate a gaussian random field map from input cls and mask
    - cl_dict is a dictionary of C(l)s, evaluated at integer ell which should start from 0
    - mask is a binary array (0==masked)"""

    def __init__(self,nside, ell, cl_dict, weight_map=None):

        self.nside=nside
        self.npix=hp.nside2npix(self.nside)
        self.pix_nums=np.arange(self.npix)
        self.thetas,self.phis=hp.pix2ang(self.nside,self.pix_nums)

        self.ell=np.array(ell)
        assert self.ell[0]==0

        #healpy wants a list of c_ells,  TT, EE, BB, TE, EB, TB - make sure to set new=True in synalm, synfast etc.
        self.c_ells=[]
        cl_keys = ['TT','EE','BB','TE','EB','TB']
        if not any(key in cl_dict for key in cl_keys):
            print(cl_dict)
            raise KeyError("No signal provided, input dictionary printed above")
        for key in ['TT','EE','BB','TE','EB','TB']:
            try:
                self.c_ells.append(cl_dict[key])
            except KeyError:
                #print "no dict entry for %s"%key
                self.c_ells.append(np.zeros_like(self.ell))

        self.weight_map=weight_map

    def set_rect_mask(self, theta_min, theta_max, phi_min, phi_max):
        mask=np.zeros(self.npix)
        masked=np.ones(self.npix,dtype=bool)
        in_range=(self.thetas<theta_min)*(self.thetas>theta_max)*(self.phis>phi_min)*(self.phis<phi_max)
        self.mask[in_range]=1
        self.masked[in_range]=False

    def realise_cls(self, ret_maps=False, lmax=None, comm=None, num_real=5, noise_map0=None, noise_map2=None, sub_noise_cls=None):
        """
        - noise_map0 and noise_map2 should be spin 0 and spin 2 NoiseMap instances. noise_map0 will be used to generate noise
        for spin 0 maps (e.g. temperature), and noise_map2 will be used for spin 2 maps (e.g. shear).
        - noise_sub_cls should be noise cls to subtract...this all seems a bit circular,
        but should do same as we do on the data...so generate noisy maps, and subtract pre-calculated noise cls
        - if ret_maps, return maps as well as Cls.
        - Provide an mpi4py comm object if you want to mpi dat shit"""
        if lmax is None:
            lmax=self.ell[-1]

        if comm is None:
            master=True
            cls_list=[]
            if ret_maps:
                map_list=[]
            for i_real in range(num_real):
                np.random.seed(random_seed(i_real))
                maps = self.realise_map(noise_map0=noise_map0, noise_map2=noise_map2, ret_raw_maps=True)
                alms_cut=hp.map2alm(maps, lmax=lmax, pol=True)
                c_ells=hp.alm2cl(alms_cut)
                c_ells=healpy_list_to_dict(c_ells)
                if sub_noise_cls is not None:
                    c_ells=cl_dict_sub_noise(c_ells, sub_noise_cls)
                cls_list.append(c_ells)
                if ret_maps:
                    map_list.append(maps)
        else:
            #heard it's frowned upon to import module in a function...but think it's justified here, as want to 
            #be able to use this library without mpi too....
            from mpi4py import MPI
            from mpi4py.MPI import ANY_SOURCE
            rank=comm.Get_rank()
            size=comm.Get_size()

            if rank==0:
                master=True
                cls_list=[]
                if ret_maps:
                    map_list=[]
                num_rec=0
                while num_rec < num_real:
                    cls = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG)
                    if ret_maps:
                        cls_list.append(cls[0])
                        map_list.append(cls[1])
                    else:
                        cls_list.append(cls)
                    num_rec+=1

            else:
                np.random.seed(random_seed(rank))
                for i in range(num_real):
                    if i%(size-1)!=rank-1:
                        continue
                    maps = self.realise_map(noise_map0=noise_map0, noise_map2=noise_map2, ret_raw_maps=True)
                    alms_cut = hp.map2alm(maps, lmax=lmax, pol=True)
                    c_ells=hp.alm2cl(alms_cut)
                    c_ells=healpy_list_to_dict(c_ells)
                    if sub_noise_cls is not None:
                        c_ells=cl_dict_sub_noise(c_ells, sub_noise_cls)
                    if ret_maps:
                        comm.send((c_ells,maps), dest=0)
                    else:
                        comm.send(c_ells, dest=0)
                return 0
        if master:
            if ret_maps:
                return cls_list,map_list
            else:
                return cls_list
                                      
            
    def realise_map(self, noise_map0=None, noise_map2=None, ret_raw_maps=False, T_only=False, P_only=False, mask=None, 
                    apply_weight=True, apply_weight_to_noise=False):
        """
        - Realise maps from self.c_ells. 
        - Optionally provide spin-0 or spin-2 noise_maps as noise_map0 and noise_map2.
        - Set T_only or P_only if you only want the T or the Q,U maps respectively
        """
        #Set zero noise maps as default
        noise_real0=Map(self.nside, np.zeros(self.npix))
        noise_real2=Map(self.nside, np.zeros(self.npix), map2=np.zeros(self.npix))

        #generate noiseless, full sky maps
        alms=hp.synalm(self.c_ells, new=True)
        maps=hp.alm2map(alms, nside=self.nside, pixwin=True)
        #maps is a list of 3 maps - e.g. T,Q,U or kappa,g1,g2 etc.

        #generate noise
        if noise_map0 is not None:
            noise_real0=noise_map0.realise()
        if noise_map2 is not None:
            noise_real2=noise_map2.realise()

        #do masking and add noise - make a 'T,Q,U' (kappa, g1, g2) list of noise maps
        noise_map_list=[noise_real0.map1, noise_real2.map1, noise_real2.map2]
        ms_masked_noisy=[]
        for m,n in zip(maps,noise_map_list):
            m_masked=m.copy()
            if apply_weight:
                m_masked*=self.weight_map
            if apply_weight_to_noise:
                n*=self.weight_map
            m_masked+=n
            ms_masked_noisy.append(m_masked)
        if ret_raw_maps:
            return ms_masked_noisy
        else:
            if T_only:
                return Map(self.nside, ms_masked_noisy[0])
            elif P_only:
                return Map(self.nside, ms_masked_noisy[1], map2=ms_masked_noisy[2], weight_map=self.weight_map)
            else:
                return (Map(self.nside, ms_masked_noisy[0]), Map(self.nside, ms_masked_noisy[1], map2=ms_masked_noisy[2]))


def maps2cldict(map_1, lmax=None, map_2=None):
    """correlate one or two maps, return dictionary of Cls"""
    #First check inputs
    nside=map_1.nside
    if map_2 is not None:
        assert map_2.nside==nside

    if map_1.spin==0:
        maps_1=(map_1.map1, np.zeros_like(map_1.map1), np.zeros_like(map_1.map1))
    else:
        maps_1=(np.zeros_like(map_1.map1),map_1.map1,map_1.map2)
    if map_2 is None:
        cls_list=hp.anafast(maps_1, lmax=lmax)
    else:
        if map_2.spin==0:
            maps_2=(map_2.map1, np.zeros_like(map_2.map1), np.zeros_like(map_2.map1))
        else:
            maps_2=(np.zeros_like(map_2.map1),map_2.map1,map_2.map2)

        cls_list = hp.anafast(maps_1, map2=maps_2, lmax=lmax)
    
    cls = healpy_list_to_dict(cls_list)
    return cls

def get_count_map(ras, decs, threshold=0, weights=None, nside=1024, scheme="ring"):
    
    hpix = hu.HealPix(scheme,nside)
    pixinds = np.arange(hpix.npix)

    pixnum_arr = hpix.eq2pix(ras,decs)
    count_map = np.zeros(hpix.npix)

    pix_bin_count=np.bincount(pixnum_arr, minlength=hpix.npix, weights=weights)
    pixinds_nonzero=pixinds[(pix_bin_count>threshold)]
    pixinds_zero=pixinds[(pix_bin_count<=threshold)]
    mask=np.zeros(hpix.npix)
    mask[pixinds_nonzero]=1.
    #sort input arrays by pixel number, then use np.searchsorted to find indices of each remaining pixel index in pixnum_arr
    sort_inds=np.argsort(pixnum_arr)
    pixnum_arr = pixnum_arr[sort_inds]
    if weights is not None:
        weights=weights[sort_inds]
    else:
        weights=np.ones_like(pixnum_arr)
    insert_inds=np.searchsorted(pixnum_arr, pixinds_nonzero)
    #insert_inds[i],insert_inds[i+1] is now the start and finish index in pixnum_arr for each pixel numer in pixinds_nonzero,
    #except for the largest, so add len(pixnum_arr)+1 to insert_inds
    insert_inds=np.concatenate((insert_inds, np.array([len(pixnum_arr)])))
    print('%d/%d non-zero pixels'%(len(pixinds_nonzero), len(pixinds)))

    for i,pixind in enumerate(pixinds_nonzero):
        inds=np.arange(insert_inds[i],insert_inds[i+1])
        count_map[pixind]=np.sum(weights[inds])

    return count_map, mask
                                   
class ShearMapsFromCat(object):

    def __init__(self, nside, mask=None):
        self.nside=nside
        self.hpix=hu.HealPix("ring",self.nside)
        self.npix=self.hpix.npix
        self.pixinds=np.arange(self.npix)
        print('shear map with nside %d'%nside)

    def arr2map(self, arr_data, ra_col='ra', dec_col='dec', spin0_cols=None, spin2_cols=[('e1','e2'),('g1','g2')], 
                get_std_maps=True, threshold=0, shear_tot_noise=True, weight_col=None, flip_g2=False):
        """This is the funtion that generates the maps from a numpy recarray (arr_data)
        - Supply a dict of array columns, which at the least has the ra and dec column names i.e. {'ra':'<ra column name>', 'dec':'<dec column name>'}
        - Will generate maps (and noise maps) for the columns you ask for in spin0_cols and spin2_cols
        - Provide spin2_cols as tuple pairs
        - Will also generate 'noise' maps - this is the error on the mean in each pixel
        - Provide a threshold to mask pixels with less than this number of counts
        - Returns mask, count_map, list of spin 0 maps, list of spin 0 noise maps, list of spin 2 maps, list of spin 2 noise maps
        - If shear_tot_noise is True, calculate shape noise map as follow: 
        i) for a given pixel, compute sigma_e^2 = \Sum_i w_i (e1^2 + e2^2) / \Sum_i w_i
        ii) Then sigma_e_1 = sigma_e_2 = sqrt(sigma_e^2 / 2)"""

        cols2map=[spin0_cols]+[spin2[0] for spin2 in spin2_cols]+[spin2[1] for spin2 in spin2_cols] #get a list of all columns you want to map
        cols2map=[col for col in cols2map if col is not None] #remove None
        print(cols2map)

        #weights
        if weight_col:
            weights = arr_data[weight_col]
        else:
            weights = np.ones(len(arr_data))

        npix=self.npix
        ra,dec=arr_data[ra_col],arr_data[dec_col]
        pixnum_arr = self.hpix.eq2pix(ra,dec)
        
        maps=[]
        maps_sq=[]
        map_names=[]
        for col in cols2map:
            assert col in arr_data.dtype.names
            maps.append(np.zeros(self.npix))
            maps_sq.append(np.zeros(self.npix))
            map_names.append(col)

        count_map=np.zeros(self.npix)
        countsq_map=np.zeros(self.npix)
        print('making the following maps:',map_names)
            
        #this can get pretty slow for large maps...but the following should be ok
        #first do a bincount to work out which pixels are empty
        pix_bin_count=np.bincount(pixnum_arr, minlength=npix)
        pixinds_nonzero=self.pixinds[(pix_bin_count>threshold)]
        pixinds_zero=self.pixinds[(pix_bin_count<=threshold)]
        mask=np.zeros(self.npix)
        mask[pixinds_nonzero]=1.
        #sort input arrays by pixel number, then use np.searchsorted to find indices of each remaining pixel index in pixnum_arr
        sort_inds=np.argsort(pixnum_arr)
        arr_data,pixnum_arr=arr_data[sort_inds],pixnum_arr[sort_inds]
        insert_inds=np.searchsorted(pixnum_arr, pixinds_nonzero)

        #insert_inds[i],insert_inds[i+1] is now the start and finish index in pixnum_arr for each pixel numer in pixinds_nonzero,
        #except for the largest, so add len(pixnum_arr)+1 to insert_inds
        insert_inds=np.concatenate((insert_inds, np.array([len(pixnum_arr)])))
        print('%d/%d non-zero pixels'%(len(pixinds_nonzero), len(self.pixinds)))

        col_data=[]
        for c in map_names:
            col_data.append(arr_data[c])

        #Shape noise if requested
        if shear_tot_noise:
            e_sq_maps=[]
            e_sq_arrs=[]
            for spin2 in spin2_cols:
                e_sq_maps.append(np.zeros(self.npix))
                e_sq_arrs.append(arr_data[spin2[0]]**2 + arr_data[spin2[1]]**2)

        #Now just need to loop through the non-zero pixel indices adding to appropriate range of values from the input catalog.
        for i,pixind in enumerate(pixinds_nonzero):
            inds=np.arange(insert_inds[i],insert_inds[i+1])
            for j,c in enumerate(col_data):
                maps[j][pixind]=np.sum(c[inds]*weights[inds])
                maps_sq[j][pixind]=np.sum(weights[inds]**2 * c[inds]**2)
            if shear_tot_noise:
                for j,c in enumerate(e_sq_arrs):
                    e_sq_maps[j][pixind] = np.sum(c[inds]*weights[inds])  #think this should be weight^2....?
            count_map[pixind]=np.sum(weights[inds])
            countsq_map[pixind]=np.sum(weights[inds]**2)

        map_dict={}
        map_err_mean_dict={}
        
        #Loop through maps
        for name,m,m2 in zip(map_names,maps,maps_sq):
            m_bar=m/count_map
            map_dict[name]=m_bar
            map_dict[name][pixinds_zero]=hp.UNSEEN
            map_err_mean_dict[name]=np.sqrt((m2-count_map*m_bar**2)/(count_map-1))/np.sqrt(count_map)
            map_err_mean_dict[name][pixinds_zero]=hp.UNSEEN

        map_dict['count']=count_map
        noise_maps={}

        spin2_maps,spin2_noise_maps=None,None
        if spin2_cols is not None:
            spin2_maps={}
            spin2_noise_maps={}
            for j,spin2 in enumerate(spin2_cols):
                if flip_g2:
                    spin2_maps[spin2]=Map(self.nside, map_dict[spin2[0]], map2=-map_dict[spin2[1]])
                else:
                    spin2_maps[spin2]=Map(self.nside, map_dict[spin2[0]], map2=map_dict[spin2[1]])
                if shear_tot_noise:
                    mean_e_sq_map = e_sq_maps[j]/count_map
                    #!!!this is a biased std estimator....
                    #mean_e_sq_map /= (1-countsq_map/count_map**2)
                    mean_e_sq_map[pixinds_zero]=hp.UNSEEN
                    sigma_e_i_map = np.sqrt(mean_e_sq_map/2/count_map)  #factor of 2 here because we've used total e in calculation of e_sq_arrs.
                    spin2_noise_maps[spin2]=NoiseMap(self.nside, sigma_e_i_map, map2=sigma_e_i_map)
                else:
                    spin2_noise_maps[spin2]=NoiseMap(self.nside, map_err_mean_dict[spin2[0]], map2=map_err_mean_dict[spin2[1]])
                
        spin0_maps,spin0_noise_maps=None,None
        if spin0_cols is not None:
            spin0_maps={}
            spin0_noise_maps={}
            for spin0 in spin0_cols:
                spin0_maps[spin0]=Map(self.nside, map_dict[spin0])
                spin0_noise_maps[spin0]=NoiseMap(self.nside, map_err_mean_dict[spin0])

        return mask, count_map, spin0_maps, spin0_noise_maps, spin2_maps, spin2_noise_maps
