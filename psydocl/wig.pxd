cdef extern from "wigxjpf.h" nogil:
    void wig_table_init(int, int)
    void wig_table_free()
    void wig_temp_init(int)
    void wig_thread_temp_init(int)
    void wig_temp_free()
    double wig3jj(int, int, int,
              int, int, int)
       