"""Tools to compute pseudoCl mixing matrices to recover unbiased angular power spectra,
C(l), from  masked sky C(l). Follows the notation in Hikage et al. 2010 http://arxiv.org/abs/1004.3542
"""
import healpy as hp
import scipy.interpolate as interp
import numpy as np
import wignerpy._wignerpy as wp
from . import mixing_c

try:
    from mpi4py import MPI
    from mpi4py.MPI import ANY_SOURCE
except ImportError:
    print("failed to import mpi4py - mpi methods won't work")
    pass


def get_curvy_k(mask_map,lmax, mask_map2=None):

    K_lm1=hp.sphtfunc.map2alm(mask_map,lmax=lmax)
    K_lm1_star=np.conj(K_lm1)
    if mask_map2 is not None:
        K_lm2=hp.sphtfunc.map2alm(mask_map2, lmax=lmax)
        K_lm2_star=np.conj(K_lm2)
    else:
        K_lm2,K_lm2_star=K_lm1,K_lm1_star
    alm_obj=hp.Alm()
    size=alm_obj.getsize(lmax)
    alm_ls,alm_ms=alm_obj.getlm(lmax,np.arange(size))
    ms=np.unique(alm_ms)
    ls=np.arange(lmax+1)
    curvey_ks=np.zeros(lmax+1)

    m_eq_0 = (alm_ms==0)
    for l in ls:
        is_l=(alm_ls==l)
        is_l_and_m_eq_0=is_l*m_eq_0
        #sum over m of K_lm1*K_lm2_star.
        curvey_ks[l] = np.sum(K_lm1[is_l]*K_lm2_star[is_l]) + np.sum(K_lm1_star[is_l]*K_lm2[is_l]) - np.real((K_lm1[is_l_and_m_eq_0]*K_lm2_star[is_l_and_m_eq_0])[0])
        #curvey_ks[l] = 2 * np.sum(K_lm1[is_l]*K_lm2_star[is_l]) - (K_lm1[is_l_and_m_eq_0]*K_lm2_star[is_l_and_m_eq_0])[0]

    curvey_ks/=(2*ls+1)
    return curvey_ks

def get_mixing_EE(lmin, lmax, curvy_k):
    ls=np.arange(lmin,lmax+1)
    num_l=len(ls)
    l_inds=np.arange(num_l)
    lks=np.arange(0,lmax+1)
    M=np.zeros((num_l,num_l))
    for i,li in zip(l_inds,ls):
        for j,lj in zip(l_inds,ls):
            prefactor= (2*lj+1.)/8/np.pi
            lk_sum=0
            for lk in lks:
                add_to_sum=(2*lk + 1) * curvy_k[lk] * (1 + (-1)**(li+lj+lk)) * (wp.wigner3j(li,lj,lk,2,-2,0))**2
                #print li,lj,lk,add_to_sum, Wigner3j(li,lj,lk,2,-2,0)
                lk_sum+=add_to_sum
            M[i,j]=prefactor*lk_sum
    return M

class MixingMatrixFull(object):

    def __init__(self, lmin, lmax, mask_T=None, mask_P=None, mask_T2=None, mask_P2=None, curvy_k=None):
        #lmin and lmax are the minimum lmin and lmax you want to calculate the corrected power spectrum for
        #high_l is the maximum l used to calculate this (since C(l)s outside [lmin,lmax] will contribute
        #to the masked C(l)
        self.lmin=lmin
        self.lmax=lmax
        self.ls=np.arange(lmin,lmax+1)
        self.lks=np.arange(lmax+1)
        self.num_l=len(self.ls)
        self.l_inds=np.arange(self.num_l)
        self.Ms={} #dictionary of mixing matrices
        if curvy_k is not None:
            self.curvy_k_TT=curvy_k
            self.curvy_k_TP=curvy_k
            self.curvy_k_PP=curvy_k
        else:
            if mask_T is not None:
                self.curvy_k_TT=get_curvy_k(mask_T, self.lmax, mask_map2=mask_T2)
                if mask_P is None:
                    self.curvy_k_PP=self.curvy_k_TT
                    self.curvy_k_TP=self.curvy_k_TT
                else:
                    self.curvy_k_TP=get_curvy_k(mask_T, self.lmax, mask_map2=mask_P)
            if mask_P is not None:
                self.curvy_k_PP=get_curvy_k(mask_P, self.lmax, mask_map2=mask_P2)

        self.ells=np.arange(self.lmin, self.lmax+1)

    def mixing_worker(self, li):
        M_TT_TT_li=np.zeros(self.num_l)
        M_TE_TE_li=np.zeros(self.num_l)
        M_EE_EE_li=np.zeros(self.num_l)
        M_EE_BB_li=np.zeros(self.num_l)
        M_EB_EB_li=np.zeros(self.num_l)
        for j,lj in zip(self.l_inds,self.ls):
            prefactor= (2*lj+1.)/8/np.pi
            TT_TT_lk_sum=0
            TE_TE_lk_sum=0
            EE_EE_lk_sum=0
            EE_BB_lk_sum=0
            EB_EB_lk_sum=0

            for lk in self.lks:
                if self.TT or self.TP:
                    temp_wigner = wp.wigner3j(li,lj,lk,0,0,0)
                    TT_TT_lk_sum+= (2*lk + 1) * self.curvy_k_TT[lk] * temp_wigner**2
                if self.TP or self.PP:
                    pol_wigner = wp.wigner3j(li,lj,lk,2,-2,0)
                if self.TP:
                    TE_TE_lk_sum+= (2*lk + 1) * self.curvy_k_TP[lk] * temp_wigner * pol_wigner 
                if self.PP:
                    add_to_sum=(2*lk + 1) * self.curvy_k_PP[lk] * pol_wigner**2
                    EE_EE_lk_sum+= (1 + (-1)**(li+lj+lk)) * add_to_sum
                    EE_BB_lk_sum+= (1 - (-1)**(li+lj+lk)) * add_to_sum
                    EB_EB_lk_sum+= add_to_sum  

            M_EE_EE_li[j]=prefactor*EE_EE_lk_sum
            M_EE_BB_li[j]=prefactor*EE_BB_lk_sum
            M_EB_EB_li[j]=2*prefactor*EB_EB_lk_sum #factor of 2 because of 4*pi instead of 8*pi in eqn 14 if hikage
            M_TT_TT_li[j]=2*prefactor*TT_TT_lk_sum
            M_TE_TE_li[j]=2*prefactor*TE_TE_lk_sum
            
        return M_TT_TT_li, M_TE_TE_li, M_EE_EE_li, M_EE_BB_li, M_EB_EB_li

    def get_mixing_single(self, TT_TP_PP, c=False):
        if TT_TP_PP=="TT":
            if c:
                M = get_mixing_TT_wigxjpf(self.lmin, self.lmax, self.curvy_k_TT)
            else:
                M = get_mixing_TT(self.lmin, self.lmax, self.curvy_k_TT)
            self.Ms['TT_TT'] = M
        elif TT_TP_PP=="TP":
            if c:
                M = get_mixing_TP_wigxjpf(self.lmin, self.lmax, self.curvy_k_TP)
            else:
                M = get_mixing_TP(self.lmin, self.lmax, self.curvy_k_TP)
            self.Ms['TE_TE'] = M
        else:
            assert TT_TP_PP=='PP'
            if c:
                Ms = mixing_c.get_mixing_PP_wigxjpf(self.lmin, self.lmax, self.curvy_k_PP)
            else:
                Ms = get_mixing_PP(self.lmin, self.lmax, self.curvy_k_PP)
            self.Ms['EE_EE'], self.Ms['EE_BB'], self.Ms['EB_EB'] = Ms
        self.keys=self.Ms.keys()
        self.num_Ms=len(self.keys)
        return self.Ms


    def get_mixing_single_clip(self, TT_TP_PP, clip=0.01):
        if TT_TP_PP=="TT":
            M = get_mixing_TT_wigxjpf(self.lmin, self.lmax, self.curvy_k_TT)
            self.Ms['TT_TT'] = M
        elif TT_TP_PP=="TP":
            M = get_mixing_TP_wigxjpf(self.lmin, self.lmax, self.curvy_k_TP)
            self.Ms['TE_TE'] = M
        else:
            assert TT_TP_PP=='PP'
            Ms = mixing_c.get_mixing_PP_wigxjpf_clip(self.lmin, self.lmax, self.curvy_k_PP, clip)
            self.Ms['EE_EE'], self.Ms['EE_BB'], self.Ms['EB_EB'] = Ms
        self.keys=self.Ms.keys()
        self.num_Ms=len(self.keys)
        return self.Ms

    def get_mixing(self, TT=True, TP=True, PP=True):

        self.num_lks=len(self.lks)
        lk_inds=self.lks
        self.keys=[]
        self.TT,self.TP,self.PP = TT,TP,PP
        if TT:
            self.keys+=['TT_TT']
        if TP:
            self.keys+=['TE_TE']
        if PP:
            self.keys+=['EE_EE','EE_BB','EB_EB']
        self.num_Ms=len(self.keys)

        M_finals={}
        for key in self.keys:
            M_finals[key] = np.zeros((self.num_l,self.num_l))

        for i,li in zip(self.l_inds,self.ls):
            M_TT_TT_li, M_TE_TE_li, M_EE_EE_li, M_EE_BB_li, M_EB_EB_li=self.mixing_worker(li)
            if TT:
                M_finals['TT_TT'][i] = M_TT_TT_li
            if TP:
                M_finals['TE_TE'][i] = M_TE_TE_li
            if PP:
                M_finals['EE_EE'][i] = M_EE_EE_li
                M_finals['EE_BB'][i] = M_EE_BB_li
                M_finals['EB_EB'][i] = M_EB_EB_li

        self.Ms = M_finals
        return M_finals

    def get_mixing_mpi(self,comm, TT=True, TP=True, PP=True):

        if comm is None:
            return self.get_mixing(TT=TT, TP=TP, PP=PP)

        self.num_lks=len(self.lks)
        lk_inds=self.lks
        rank=comm.Get_rank()
        size=comm.Get_size()
        self.keys=[]
        if TT:
            self.keys+=['TT_TT']
        if TP:
            self.keys+=['TE_TE']
        if PP:
            self.keys+=['EE_EE','EE_BB','EB_EB']
        self.num_Ms=len(self.keys)

        if rank==0:
            print('starting parallel M calc')
            #print 'rank 0 starting parallel M calc'
            M_finals={}
            for key in self.keys:
                M_finals[key] = np.zeros((self.num_l,self.num_l))

            lis_received=0
            while lis_received<self.num_l:
                rec_data=comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG)
                i, M_TT_TT_li, M_TE_TE_li, M_EE_EE_li, M_EE_BB_li, M_EB_EB_li=rec_data
                if TT:
                    M_finals['TT_TT'][i] = M_TT_TT_li
                if TP:
                    M_finals['TE_TE'][i] = M_TE_TE_li
                if PP:
                    M_finals['EE_EE'][i] = M_EE_EE_li
                    M_finals['EE_BB'][i] = M_EE_BB_li
                    M_finals['EB_EB'][i] = M_EB_EB_li
                lis_received+=1
            
            self.Ms = M_finals
            return M_finals
        else:
            #print 'rank %d starting parallel M calc'%rank
            #split lis between cores
            for i,li in zip(self.l_inds,self.ls):
                if i%(size-1)!=rank-1:
                    continue
                M_TT_TT_li=np.zeros(self.num_l)
                M_TE_TE_li=np.zeros(self.num_l)
                M_EE_EE_li=np.zeros(self.num_l)
                M_EE_BB_li=np.zeros(self.num_l)
                M_EB_EB_li=np.zeros(self.num_l)
                for j,lj in zip(self.l_inds,self.ls):
                    prefactor= (2*lj+1.)/8/np.pi
                    TT_TT_lk_sum=0
                    TE_TE_lk_sum=0
                    EE_EE_lk_sum=0
                    EE_BB_lk_sum=0
                    EB_EB_lk_sum=0

                    for lk in self.lks:
                        if TT or TP:
                            temp_wigner = wp.wigner3j(li,lj,lk,0,0,0)
                            TT_TT_lk_sum+= (2*lk + 1) * self.curvy_k_TT[lk] * temp_wigner**2
                        if TP or PP:
                            pol_wigner = wp.wigner3j(li,lj,lk,2,-2,0)
                        if TP:
                            TE_TE_lk_sum+= (2*lk + 1) * self.curvy_k_TP[lk] * temp_wigner * pol_wigner 
                        if PP:
                            add_to_sum=(2*lk + 1) * self.curvy_k_PP[lk] * pol_wigner**2
                            EE_EE_lk_sum+= (1 + (-1)**(li+lj+lk)) * add_to_sum
                            EE_BB_lk_sum+= (1 - (-1)**(li+lj+lk)) * add_to_sum
                            EB_EB_lk_sum+= add_to_sum  

                    M_EE_EE_li[j]=prefactor*EE_EE_lk_sum
                    M_EE_BB_li[j]=prefactor*EE_BB_lk_sum
                    M_EB_EB_li[j]=2*prefactor*EB_EB_lk_sum #factor of 2 because of 4*pi instead of 8*pi in eqn 14 if hikage
                    M_TT_TT_li[j]=2*prefactor*TT_TT_lk_sum
                    M_TE_TE_li[j]=2*prefactor*TE_TE_lk_sum

                comm.send((i,M_TT_TT_li,M_TE_TE_li,M_EE_EE_li,M_EE_BB_li,M_EB_EB_li), dest=0)


    def bin_Ms(self, lbinner=None, num_ell_bins=10, nside=None):

        if lbinner is None:
            from cltools import ClBinner
            est_ells=np.arange(self.lmin,self.lmax+1)
            ell_splits=np.array_split(est_ells, num_ell_bins)
            ell_bin_lims=np.array([l[0] for l in ell_splits] + [ell_splits[-1][-1]+1])
            self.lbinner=ClBinner(ell_bin_lims)
        else:
            self.lbinner=lbinner

        ell_bin_lims=self.lbinner.ell_bin_lims
        self.num_bins=self.lbinner.num_ell_bins

        pix=True
        if nside is None:
            print('no nside given, not doing pixel (de)convolution')
            pix=False
        if pix:
            pixwin=hp.pixwin(nside,pol=True)
            pixwin_T, pixwin_P = pixwin
            assert len(pixwin_P)>self.lmax
            
        assert ell_bin_lims[0]>=self.lmin
        assert ell_bin_lims[-1]<=self.lmax+1
        #self.num_bins = len(ell_bin_lims)-1

        Ms_list = [self.Ms[key] for key in self.keys]
        Ms_binned = [np.zeros((self.num_bins,self.num_bins)) for i in range(self.num_Ms)]
        
        bin_ells = self.lbinner.bin_ells
        bin_ell_inds = self.lbinner.bin_ell_inds
        
        for bin_i in range(self.num_bins):
            ells_i,inds_i = bin_ells[bin_i], bin_ell_inds[bin_i]
            P_bl = ells_i.astype(float)*(ells_i+1)/2/np.pi/(len(ells_i))
            for bin_j in range(self.num_bins):
                ells_j,inds_j = bin_ells[bin_j], bin_ell_inds[bin_j]
                ell_i_sums = [np.zeros(len(ells_i)) for i in range(self.num_Ms)]
                for i,(ell_i,ind_i) in enumerate(zip(ells_i, inds_i)):   #Here ell_i is ell in bin_i, ind_i is the index of that ell in M, and i is the index of that ell in bin_i
                    #print ind_i, inds_j
                    Mljs = [M[ind_i, inds_j] for M in Ms_list]
                    Q_lbs=2*np.pi/ells_j/(ells_j+1)
                    if pix:
                        Q_lbs = Q_lbs * pixwin_P[ells_j]**2
                        #print 'warning: sort out T pixwin'

                    for iM in range(self.num_Ms):
                        ell_i_sums[iM][i] = np.sum(Mljs[iM] * Q_lbs)

                for iM in range(self.num_Ms):
                    Ms_binned[iM][bin_i, bin_j] = np.sum(P_bl * ell_i_sums[iM])

        self.Ms_binned = {}
        for key,M in zip(self.keys, Ms_binned):
            self.Ms_binned[key] = M

        return self.Ms_binned

    def get_EE_EE_BB_BB_binned(self):
        """Get the combined EE_EE and BB_BB mixing matrix"""
        M = np.zeros((2*self.num_bins, 2*self.num_bins))
        M[:self.num_bins, :self.num_bins] = self.Ms_binned['EE_EE']
        M[self.num_bins:, self.num_bins:] = self.Ms_binned['EE_EE']
        M[:self.num_bins, self.num_bins:] = self.Ms_binned['EE_BB']
        M[self.num_bins:, :self.num_bins] = self.Ms_binned['EE_BB']
        return M
        
    def binned_inversion(self, cls):
        """supply a dictionary of cls, with keys in ['TT','TE','TB','EE','BB','EB']"""
        cls_binned={}
        for key,val in cls.iteritems():
            cls_binned[key] = self.lbinner.bin_cl(self.lks, val[:self.lmax+1])
        cls_out={}
        self_mixing_keys = ['TT','TE','TB','EB'] #these fellas keep the mixing to themselves
        for key in self_mixing_keys:
            try:
                cls_out[key] = np.linalg.solve(self.Ms_binned[key+'_'+key], cls_binned[key])
            except KeyError:
                continue

        #EE and BB mix with eachother so need to invert together
        if 'EE' in cls_binned:
            if 'BB' in cls_binned:
                cl_ee_bb = np.zeros(2*self.num_bins)
                cl_ee_bb[:self.num_bins] = cls_binned['EE']
                cl_ee_bb[self.num_bins:] = cls_binned['BB']
                M_ee_bb = self.get_EE_EE_BB_BB_binned()
                cl_ee_bb_solved = np.linalg.solve(M_ee_bb, cl_ee_bb)
                cls_out['EE'] = cl_ee_bb_solved[:self.num_bins]
                cls_out['BB'] = cl_ee_bb_solved[self.num_bins:]
            else:
                cls_out['EE'] = np.linalg.solve(self.Ms_binned['EE_EE'], cls_binned['EE'])

        elif 'BB' in cls_binned:
            cls_out['BB'] = np.linalg.solve(self.Ms_binned['BB_BB'], cls_binned['BB'])

        return cls_out

def get_mixing_TT(lmin, lmax, curvy_k):

    ls = np.arange(lmin,lmax+1)
    lks = np.arange(lmax+1)
    num_lks=len(lks)
    lk_inds=lks
    num_l=lmax+1-lmin

    M = np.zeros((num_l,num_l))
    for i in range(num_l):
        li = ls[i]
        for j in range(num_l):
            lj =  ls[j]
            prefactor= (2*lj+1.)/8/np.pi
            TT_TT_lk_sum=0.
            for lk in lks:
                temp_wigner = wp.wigner3j(li,lj,lk,0,0,0)
                TT_TT_lk_sum += (2*lk + 1.) * curvy_k[lk] * temp_wigner**2
            M[i,j] = 2*prefactor*TT_TT_lk_sum

    return M

def get_mixing_TP(lmin, lmax, curvy_k):

    ls = np.arange(lmin,lmax+1)
    lks = np.arange(lmax+1)
    num_lks=len(lks)
    lk_inds=lks
    num_l=lmax+1-lmin

    M = np.zeros((num_l,num_l))
    for i in range(num_l):
        li = ls[i]
        for j in range(num_l):
            lj =  ls[j]
            prefactor= (2*lj+1.)/8/np.pi
            TE_TE_lk_sum=0.
            for lk in lks:
                temp_wigner = wp.wigner3j(li,lj,lk,0,0,0)
                pol_wigner = wp.wigner3j(li,lj,lk,2,-2,0)
                TE_TE_lk_sum+= (2*lk + 1.) * curvy_k[lk] * temp_wigner * pol_wigner 
            M[i,j] = 2*prefactor*TE_TE_lk_sum

    return M


#import sys
#sys.path.append('/Users/maccrann.2/code/wigxjpf-1.6/pywigxjpf')
#import pywigxjpf
def get_mixing_PP(lmin, lmax, curvy_k):

    ls = np.arange(lmin,lmax+1)
    lks = np.arange(lmax+1)
    num_lks=len(lks)
    lk_inds=lks
    num_l=lmax+1-lmin

    #pywigxjpf.wig_temp_init(2*lmax)
    #pywigxjpf.wig_table_init(2*lmax, wigner_type=3)

    M_EE_EE = np.zeros((num_l,num_l))
    M_EE_BB = np.zeros((num_l,num_l))
    M_EB_EB = np.zeros((num_l,num_l))
    
    for i in range(num_l):
        li = ls[i]
        for j in range(num_l):
            lj =  ls[j]
            prefactor= (2*lj+1.)/8/np.pi
            EE_EE_lk_sum=0.
            EE_BB_lk_sum=0.
            EB_EB_lk_sum=0.
            for lk in lks:
                pol_wigner = wp.wigner3j(li,lj,lk,2,-2,0)
                #pol_wigner = pywigxjpf.wig3jj([2*li, 2*lj, 2*lk, 4, -4, 0])
                add_to_sum=(2*lk + 1) * curvy_k[lk] * pol_wigner**2
                EE_EE_lk_sum+= (1 + (-1)**(li+lj+lk)) * add_to_sum
                EE_BB_lk_sum+= (1 - (-1)**(li+lj+lk)) * add_to_sum
                EB_EB_lk_sum+= add_to_sum    

            M_EE_EE[i,j] = prefactor*EE_EE_lk_sum
            M_EE_BB[i,j] = prefactor*EE_BB_lk_sum
            M_EB_EB[i,j] = 2*prefactor*EB_EB_lk_sum
    #pywigxjpf.wig_temp_free()
    return M_EE_EE, M_EE_BB, M_EB_EB
