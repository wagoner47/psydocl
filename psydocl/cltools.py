import numpy as np
import fitsio
from .mixing import get_curvy_k, MixingMatrixFull
import healpy as hp
from .shear_mapper import healpy_list_to_dict,overdensity_from_raw,NoiseMap
from collections import OrderedDict

DEFAULT_SPEC_NAMES={("lens","lens"):"cl_nn",("lens","source"):"cl_nG",("source","source"):"cl_GG"}
DEFAULT_SPEC_TYPES={("lens","lens"):("galaxy_position_fourier","galaxy_position_fourier"),
                    ("lens","source"):("galaxy_position_fourier","galaxy_shear_emode_fourier"),
                    ("source","source"):("galaxy_shear_emode_fourier","galaxy_shear_emode_fourier")}
SPEC_TYPE_KEYS={("galaxy_shear_emode_fourier","galaxy_shear_emode_fourier"):"EE",
                ("galaxy_position_fourier","galaxy_shear_emode_fourier"):"TE",
                ("galaxy_position_fourier","galaxy_position_fourier"):"TT"}

def zero_cl_dict(cl_dict):
    zero_dict={}
    for key in cl_dict:
        zero_dict[key]=np.zeros_like(cl_dict[key])
    return zero_dict

class SpecInfo(object):
    """Should probably add some type checking here"""
    def __init__(self, spec_name, modes, types):
        self.spec_name=spec_name
        if isinstance(modes, str):
            self.modes=[modes]
            assert len(types)==2
            self.types=[types]
        else:
            self.modes=modes
            assert len(self.modes)==len(types)
            self.types=types

DEFAULT_SPEC_INFO={("lens","lens"):SpecInfo("cl_nn","TT",("galaxy_position_fourier","galaxy_position_fourier")),
                   ("lens","source"):SpecInfo("cl_nG","TE",("galaxy_position_fourier","galaxy_shear_emode_fourier")),
                   ("source","source"):SpecInfo("cl_GG","EE",("galaxy_shear_emode_fourier","galaxy_shear_emode_fourier"))}        

def BinnedCL(object):
    """Class to hold binned cl. Initialise with dictionary of unbinned cls, and LBinning object, or inputs
    to LBinning object"""
    def __init__(ell, unbinned_cell_dict, lbinner=None, ell_bin_lims=None, lmin=None, lmax=None, num_ell_bins=None):
        if lbinner is None:
            if ell_bin_lims is None:
                est_ells=np.arange(self.lmin,self.lmax+1)
                ell_splits=np.array_split(est_ells, num_ell_bins)
                ell_bin_lims=np.array([l[0] for l in ell_splits]+[ell_splits[-1][-1]])
                self.lbinner=LBinning(ell_bin_lims)
            else:
                self.lbinner=LBinning(ell_bin_lims)
        else:
            self.lbinner=lbinner

        self.cl_dict={}
        for key,val in cell_dict.iteritems():
            self.cl_dict=self.lbinner.bin_cl(ell, val)

    def sub_noise(self, noise_binned_cl):
        """Subtract noise cls - noise_binned_cl should be another BinnedCL object with same binning"""
        cl_dict_sub={}
        for key,val in self.cl_dict.iteritems():
            cl_dict_sub[key]=val-noise_binned_cl.cl_dict[key]
        self.cl_dict = cl_dict_sub


class ClBinner(object):
    def __init__(self,ell_bin_lims=None, lmin=None, lmax=None, num_ell_bins=None, pad_ends=True, pad_frac_hi=1.5):
        """ell_bin_lims should contain first ell for each of the n bins, with the last element
        being what would be the first element of the n+1th bin (i.e. one more than the last ell
        in the nth bin"""
        self.pad_ends=pad_ends
        if ell_bin_lims is None:
            est_ells=np.arange(lmin,lmax+1)
            ell_splits=np.array_split(est_ells, num_ell_bins)
            if pad_ends:
                if lmin>2:
                    ell_splits=[np.arange(2,lmin)]+ell_splits
                if pad_frac_hi is not None:
                    ell_splits.append(np.arange(lmax+1,int(pad_frac_hi*(lmax+1))))
            ell_bin_lims=np.array([l[0] for l in ell_splits] + [ell_splits[-1][-1]+1])            

        self.ell_bin_lims=ell_bin_lims.astype(int)
        #print self.ell_bin_lims
        self.lmin=ell_bin_lims[0]
        self.lmax=ell_bin_lims[-1]-1
        self.num_ell_bins=len(ell_bin_lims)-1
        self.ell_bin_mids=0.5*(ell_bin_lims[:-1]+ell_bin_lims[1:])
        self.bin_ells=[]
        self.bin_ell_inds=[]
        for bin_i in range(self.num_ell_bins):
            ells=np.arange(self.ell_bin_lims[bin_i],self.ell_bin_lims[bin_i+1])
            self.bin_ells.append(ells)
            self.bin_ell_inds.append(ells-self.lmin)
        #print "self.bin_ells",self.bin_ells

    def gaussian_cov_auto(self, fsky, cell_binned, noise_cell_binned=0.):
        cell_binned_obs=cell_binned+noise_cell_binned
        cell_vars=np.zeros_like(cell_binned)
        for bin_i in range(self.num_ell_bins):
            ells = self.bin_ells[bin_i]
            num_ell_modes=np.sum(2*ells+1)
            cell_vars[bin_i] = 2 * cell_binned_obs[bin_i]**2 / fsky / num_ell_modes
        return cell_vars

    def gaussian_cov_tomo(self, fsky, num_z_bins, cell_binned_dict, noise_cell_binned_dict):
        
        cell_binned_obs=cell_binned+noise_cell_binned
        cell_vars=np.zeros_like(cell_binned)
        for bin_i in range(self.num_ell_bins):
            ells = self.bin_ells[bin_i]
            num_ell_modes=np.sum(2*ells+1)
            cell_vars[bin_i] = 2 * cell_binned_obs[bin_i]**2 / fsky / num_ell_modes
        return cell_vars

    def bin_cl(self, ell, cell):
        binned_cell=np.zeros(self.num_ell_bins)
        for i,(l_lo,l_hi) in enumerate(zip(self.ell_bin_lims[:-1],self.ell_bin_lims[1:])):
            use=(ell>=l_lo)*(ell<l_hi)
            ell_use,cell_use=ell[use],cell[use]
            ell_use,cell_use=ell_use.astype(float), cell_use.astype(float)
            p_bl=ell_use*(ell_use+1)/use.sum()/2/np.pi
            binned_cell[i] = np.sum(p_bl*cell_use)
        return binned_cell        

    def noise_cl_est(self, fsky, n_gal_ster, sigma_e=None):
        """sigma_e is the total intrinsic ellipticity dispersion (hence factor of 2 in denominator below)"""
        ell = np.arange(self.lmax+1)
        if sigma_e:
            n = np.ones(self.lmax+1) * sigma_e**2 / 2 / n_gal_ster
        else:
            n = np.ones(self.lmax+1) / n_gal_ster
        return self.bin_cl(ell, n)

def get_cls_new(clbinner, maps, comm=None, binned_Ms=None, share_weight=False, 
                auto_only=False, noise_maps=[], noise_reals=100, ret_2pt_specs=False,
                samples=None, weight_map_keys=[], spec_names=DEFAULT_SPEC_NAMES,
                spec_types=DEFAULT_SPEC_TYPES, raw_count=True, spec_info=DEFAULT_SPEC_INFO,
                verbose=True, sub_noise=True, shear_rotation_noise=False):

    """Wrapper function to estimate all auto and cross-cls from maps
    optionally supply precomputed mixing matrices.
    if raw_count=True, convert spin-0 maps (e.g. a raw count map) to overdensity maps"""
    if comm:
        rank = comm.Get_rank()
        size = comm.Get_size()
    else:
        rank=0
        size=1
    if rank==0:
        #Check there's at least one map
        #Check that maps have weight maps set
        for m in maps:
            try:
                len(m.weight_map)
            except TypeError as e:
                print("Need weight map set")
                raise e
        #If noise maps provided, check correct number of them
        if noise_maps:
            if shear_rotation_noise:
                noise_maps=[NoiseMap(m.nside, m.map1, m.map2, noise_type='RotateShear') for m in maps]
            assert len(noise_maps)==len(maps)

        field_types=[]
        pair_types_dict={(0,0):'TT', (0,2):'TP', (2,2):'PP'}
        num_maps = len(maps)
        #print [m.map1.max() for m in maps]
        #print [m.map1.min() for m in maps]

        for i,m in enumerate(maps):
            field_types.append(m.spin)
            if raw_count and m.spin==0:
                maps[i] = overdensity_from_raw(m.map1, m.nside, weight_map=m.weight_map)
        #print [m.map1.max() for m in maps]                
        #print [m.map1.min() for m in maps]

        print('field_types:',field_types)
        if samples is None:
            samples=[]
            for f in field_types:
                samples.append("lens" if f==0 else "source")

        #Get map combinations
        pair_combs=[]
        for i1 in range(num_maps):
            for i2 in range(i1,num_maps):
                if auto_only:
                    if i1!=i2: continue
                pair_combs.append((i1,i2))
        sample_combs = [(samples[i],samples[j]) for (i,j) in pair_combs]
        s=set()
        unique_sample_combs = [sc for sc in sample_combs if sc not in s and not s.add(sc)]

        #Get bin number for each map
        sample_map_nums = []
        samples_counted = []
        for s in samples:
            sample_map_nums.append(samples_counted.count(s))
            samples_counted.append(s)
        if verbose:
            print('pair_combs',pair_combs)
            print('samples',samples)
            print("sample_combs:",sample_combs)
            print("unique_sample_combs",unique_sample_combs)
            print("sample_map_nums",sample_map_nums)            

        #prepare dictionaries for output
        Ms = {}
        Cls_raw = OrderedDict()
        Cls_corrected = OrderedDict()
        Cls_noise = OrderedDict()
        Cls_noise_var = OrderedDict()
        M_arrays = {} #dict of binned mixing matrices
        Ms_cached = {}
        for sc in unique_sample_combs:
            Ms[sc] = {}
            Cls_raw[sc] = {}
            Cls_corrected[sc] = OrderedDict()
            Cls_noise[sc] = {}
            Cls_noise_var[sc] = {}
            M_arrays[sc] = {}

    else:
        pair_combs = None
        noise_maps = None
    if comm:
        if rank==0:
            print('bcasting noise maps')
        pair_combs = comm.bcast(pair_combs,root=0)
        noise_maps = comm.bcast(noise_maps, root=0)
        if rank==0:
            print('bcasting pair_combs and noise_maps: DONE')

    for pi,(i1,i2) in enumerate(pair_combs):
        curvy_k = None
        comb_type=None
        M_already_computed=False
        if rank == 0:
            sample_pair_key = (sample_map_nums[i1],sample_map_nums[i2])
            sample_comb = sample_combs[pi]
            if weight_map_keys:
                wkey1,wkey2 = weight_map_keys[i1],weight_map_keys[i2]
                try: 
                    M = Ms_cached[(wkey1,wkey2)]
                    M_already_computed=True
                    if verbose:
                        print('using cached M for pair_comb', i1,i2)
                except KeyError:
                    try:
                        M = Ms_cached[(wkey2,wkey1)]
                        M_already_computed=True
                        if verbose:
                            print('using cached M for pair_comb', i1,i2)
                    except KeyError:
                        pass
                
            #sample_comb = sample_combs[pi]
            #It may be that several bins have the same weight map, in which case for correlations between
            #these bins, you do not need to recompute the mixing matrices
            if binned_Ms:
                if sample_comb in binned_Ms:
                    M = binned_Ms[sample_comb][sample_pair_key]
                    M_already_computed=True
                    if verbose:
                        print('using precomputed M for sample_comb, bin pair', sample_comb, sample_pair_key)

            m1,m2 = maps[i1],maps[i2]
            assert m1.nside==m2.nside
            comb_type = pair_types_dict[(m1.spin,m2.spin)]
            if not M_already_computed:
                curvy_k = get_curvy_k(m1.weight_map, clbinner.lmax, mask_map2=m2.weight_map)
        if comm:
            comm.Barrier()
            comb_type = comm.bcast(comb_type, root=0)
            M_already_computed = comm.bcast(M_already_computed,root=0)

        #compute mixing matrix
        if not M_already_computed:
            curvy_k = comm.bcast(curvy_k, root=0)
            if rank==0:
                print("############################")
                print("computing mixing matrix for sample combination (%s,%s),"%(sample_comb[0],sample_comb[1]))
                print("bin combination (%d,%d)"%(sample_map_nums[i1],sample_map_nums[i2]))
                print("############################")
            M = MixingMatrixFull(clbinner.lmin, clbinner.lmax, curvy_k=curvy_k)
            if comb_type=='TT':
                if comm:
                    M.get_mixing_mpi(comm, TT=True, TP=False, PP=False)
                else:
                    M.get_mixing_single("TT", c=True)
            elif comb_type=='TP':
                if comm:
                    M.get_mixing_mpi(comm, TT=False, TP=True, PP=False)
                else:
                    M.get_mixing_single("TP",c=True)
            else:
                assert comb_type=='PP'
                if comm:
                    M.get_mixing_mpi(comm, TT=False, TP=False, PP=True)
                else:
                    M.get_mixing_single("PP",c=True)
        if comm:
            comm.Barrier()
        #bin mixing matrix
        if rank==0:

            if not M_already_computed:
                M.bin_Ms(lbinner=clbinner, nside=m1.nside)
                if weight_map_keys:
                    Ms_cached[(wkey1,wkey2)] = M
            #Compute raw Cls
            pol = (comb_type=='TP' or comb_type=='PP')
            _cls_raw = hp.sphtfunc.anafast(m1.get_healpy_input(pol=pol), m2.get_healpy_input(pol=pol), pol = pol, lmax=clbinner.lmax)
            if comb_type=='TT':
                cls_raw = {}
                cls_raw['TT'] = _cls_raw
            elif comb_type=='TP':
                cls_raw_all = healpy_list_to_dict(_cls_raw)
                cls_raw = {"TE":cls_raw_all["TE"],"TB":cls_raw_all["TB"]}
            else:
                cls_raw_all = healpy_list_to_dict(_cls_raw)
                cls_raw = {"EE":cls_raw_all["EE"],"BB":cls_raw_all["BB"],"EB":cls_raw_all["EB"]}

            #Get corrected Cls
            cls_corrected = M.binned_inversion(cls_raw)
            Ms[sample_comb][sample_pair_key] = M
            Cls_raw[sample_comb][sample_pair_key] = cls_raw
            Cls_corrected[sample_comb][sample_pair_key] = cls_corrected
            M_arrays[sample_comb][sample_pair_key] = M.Ms_binned

        #compute noise cls if noise maps are provided. Some of the noise maps may be None, in
        #in which case the noise cls are set to zero
        if noise_maps:
            if i1==i2:
                if noise_maps[i1] is not None:
                    res = noise_maps[i1].get_noise_cls(clbinner.lmax, noise_reals, comm=comm)
                    if rank==0:
                        noise_cls, noise_vars = res
                        Cls_noise[sample_comb][sample_pair_key[0]] = M.binned_inversion(noise_cls)
                else:
                    if rank==0:
                        Cls_noise[sample_comb][sample_pair_key[0]] = zero_cl_dict(cls_corrected)
    if rank!=0:
        return noise_maps
    if rank==0:
        if ret_2pt_specs:
            import twopoint
            specs,noise_specs=[],[]
            if verbose:
                print('making twopoint spec objects')
                print('unique samples:',unique_sample_combs)
            for sc in unique_sample_combs:
                print('making spec for sample comb', sc)
                n_data = len(Cls_corrected[sc]) * clbinner.num_ell_bins
                #spec_type = spec_types[sc]
                #spec_name = spec_names[sc]
                if sc not in spec_info:
                    continue
                spec_types = spec_info[sc].types
                spec_name = spec_info[sc].spec_name
                for mode, spec_type in zip(spec_info[sc].modes,spec_info[sc].types):
                    #set up arrays for input into 2pt spec object
                    cl = np.zeros(n_data)
                    noise = np.zeros(n_data)
                    zbins = np.zeros(n_data, dtype=(int,2))
                    ang_bin = np.zeros(n_data,dtype=int)
                    ang = np.zeros(n_data, dtype=(float,2))
                    k=0
                    for bin_pair in Cls_corrected[sc].keys():
                        #print Cls_corrected[sc][bin_pair]
                        inds = np.arange(k*clbinner.num_ell_bins,(k+1)*clbinner.num_ell_bins)
                        cl[inds] = Cls_corrected[sc][bin_pair][mode]
                        zbins[inds] = bin_pair 
                        ang_bin[inds] = np.arange(clbinner.num_ell_bins)
                        ang[inds] = zip(clbinner.ell_bin_lims[:-1],clbinner.ell_bin_lims[1:])
                        if noise_maps:
                            if (sc[0]==sc[1] and bin_pair[0]==bin_pair[1]):
                                noise[inds] = Cls_noise[sc][bin_pair[0]][mode]
                        k+=1
                    types = (getattr(twopoint.Types,spec_type[0]),getattr(twopoint.Types,spec_type[1]))
                    if sub_noise:
                        cl -= noise
                    spec = twopoint.SpectrumMeasurement(spec_name+"_"+mode, zbins.T+1, types, ("nz_"+sc[0],"nz_"+sc[1]),
                                                        "CLBP", ang_bin, cl, angle=ang, metadata=None)
                    specs.append(spec)
                    if noise_maps:
                        noise_spec = twopoint.SpectrumMeasurement(spec_name+"_"+mode+"_noise", zbins.T+1, types, ("nz_"+sc[0],"nz_"+sc[1]),
                                                              "CLBP", ang_bin, noise, angle=ang, metadata=None)
                        noise_specs.append(noise_spec)
                    
            return Cls_raw, Cls_corrected, Cls_noise, specs, noise_specs, Ms

        else:
            return Cls_raw, Cls_corrected, Cls_noise, Ms

def get_mixing(clbinner, weight_maps, field_types, pair_combs=None, skip_keys=None, comm=None, verbose=True, clip=None):

    if comm:
        rank = comm.Get_rank()
        size = comm.Get_size()
        from mpi4py import MPI
        from mpi4py.MPI import ANY_SOURCE
    else:
        rank=0
        size=1

    Ms=[]
    M_inds=[]
    if rank==0:

        #Check there's at least one map
        #Check that maps have weight maps set
        nside=hp.npix2nside(len(weight_maps[0]))
        for w in weight_maps:
            assert hp.npix2nside(len(w))==nside

        #get the field types - (just a list of 0 or 2 for spin-0 or spin-2 fields respectively)
        pair_types_dict={(0,0):'TT', (0,2):'TP', (2,2):'PP'}
        num_maps = len(weight_maps)
        print('field_types:',field_types)
        
        #Get map combinations
        if not pair_combs:
            pair_combs=[]
            for i1 in range(num_maps):
                for i2 in range(i1,num_maps):
                    pair_combs.append((i1,i2))

        curvy_ks = []
        comb_types = []
        for pi,(i,j) in enumerate(pair_combs):
            comb_types.append(pair_types_dict[(field_types[i], field_types[j])])
            curvy_k = get_curvy_k(weight_maps[i], clbinner.lmax, mask_map2=weight_maps[j])
            curvy_ks.append(curvy_k)

        if verbose:
            print('pair_combs',pair_combs)

    else:
        skip_keys = None
        pair_combs = None
        curvy_ks = None
        nside = None
        comb_types=None

    if comm:
        if rank==0:
            print('bcasting noise maps')
        skip_keys = comm.bcast(skip_keys, root=0)
        pair_combs = comm.bcast(pair_combs,root=0)
        curvy_ks = comm.bcast(curvy_ks, root=0)
        nside = comm.bcast(nside, root=0)
        comb_types = comm.bcast(comb_types, root=0)
        if rank==0:
            print('bcasting pairs and curvy_ks etc.')

    for pi,(i1,i2) in enumerate(pair_combs):
        if pi%size!=rank:
            continue
        if skip_keys is not None:
            if skip_keys[pi] is not None:
                continue

        comb_type = comb_types[pi] 

        #Probably best to have one rank compute all the necessary curvy_ks, since this is fast but memory intensive (not all ranks need
        #to have the maps in memory) .
        curvy_k = curvy_ks[pi]

        print("############################")
        print("rank %d computing mixing matrix for pair combination (%d,%d),"%(rank, i1, i2))
        print("############################")
        M = MixingMatrixFull(clbinner.lmin, clbinner.lmax, curvy_k=curvy_k)
        if comb_type=='TT':
            M.get_mixing_single("TT", c=True)
        elif comb_type=='TP':
            M.get_mixing_single("TP",c=True)
        else:
            assert comb_type=='PP'
            if clip:
                M.get_mixing_single_clip("PP",clip)
            else:
                M.get_mixing_single("PP",c=True)
        M.bin_Ms(lbinner=clbinner, nside=nside)
        Ms.append(M)
        M_inds.append(pi)

    if rank!=0:
        comm.send((M_inds,Ms), dest=0)
    else:
        num_Ms_current=0
        num_Ms_target=len(pair_combs)
        print('num_Ms_target',num_Ms_target)
        if skip_keys:
            for s in skip_keys:
                if s is not None:
                    num_Ms_target-=1

        num_Ms_current=len(Ms)

        if comm:
            while num_Ms_current<num_Ms_target:
                print(num_Ms_current)
                Ms_recv = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG)
                Ms += Ms_recv[1]
                M_inds += Ms_recv[0]
                num_Ms_current+=len(Ms_recv[0])

        assert num_Ms_current == num_Ms_target

        #we've done the M calculations and got a list. For map pairs with the duplicated weight-map pairs, we need to fill out the final Ms list.
        #also we need to get Ms in the right order
        Ms_out=[]
        for pi,(i1,i2) in enumerate(pair_combs):
            if skip_keys is not None:
                if skip_keys[pi] is not None:
                    Ms_out.append(Ms[M_inds.index(skip_keys[pi])])
                else:
                    Ms_out.append(Ms[M_inds.index(pi)])
            else:
                Ms_out.append(Ms[M_inds.index(pi)])
        print(Ms_out)
        print(pair_combs)
        assert len(Ms_out) == len(pair_combs)
        return Ms_out

def get_cls_openmp(clbinner, maps, comm=None, binned_Ms=None, share_weight=False, 
                auto_only=False, noise_maps=[], noise_reals=100, ret_2pt_specs=False,
                samples=None, weight_map_keys=[], spec_names=DEFAULT_SPEC_NAMES,
                spec_types=DEFAULT_SPEC_TYPES, raw_count=True, spec_info=DEFAULT_SPEC_INFO,
                   verbose=True, sub_noise=True, clip=None):

    """Wrapper function to estimate all auto and cross-cls from maps
    optionally supply precomputed mixing matrices.
    if raw_count=True, convert spin-0 maps (e.g. a raw count map) to overdensity maps
    This should do the same as get_cls_new, but uses openmp for the mixing matrix 
    part, and each mpi rank does a single mixing matrix at a time"""
    if comm:
        rank = comm.Get_rank()
        size = comm.Get_size()
        from mpi4py import MPI
        from mpi4py.MPI import ANY_SOURCE
    else:
        rank=0
        size=1

    weight_maps=[]
    field_types=[]
    if rank==0:

        #Check there's at least one map
        #Check that maps have weight maps set
        nside=maps[0].nside
        for m in maps:
            try:
                len(m.weight_map)
            except TypeError as e:
                print("Need weight map set")
                raise e
            assert m.nside==nside
            weight_maps.append(m.weight_map)
            field_types.append(m.spin)
        #If noise maps provided, check correct number of them
        if noise_maps:
            assert len(noise_maps)==len(maps)

        #get the field types - (just a list of 0 or 2 for spin-0 or spin-2 fields respectively)
        field_types=[]
        pair_types_dict={(0,0):'TT', (0,2):'TP', (2,2):'PP'}
        num_maps = len(maps)
        for i,m in enumerate(maps):
            field_types.append(m.spin)
            if raw_count and m.spin==0:
                maps[i] = overdensity_from_raw(m.map1, m.nside, weight_map=m.weight_map)


        print('field_types:',field_types)
        
        if samples is None:
            samples=[]
            for f in field_types:
                samples.append("lens" if f==0 else "source")

        #Get bin number for each map
        sample_map_nums = []
        samples_counted = []
        for s in samples:
            sample_map_nums.append(samples_counted.count(s))
            samples_counted.append(s)

        #Get map combinations
        pair_combs=[]
        for i1 in range(num_maps):
            for i2 in range(i1,num_maps):
                if auto_only:
                    if i1!=i2: continue
                pair_combs.append((i1,i2))

        sample_combs = [(samples[i],samples[j]) for (i,j) in pair_combs]
        s=set()
        unique_sample_combs = [sc for sc in sample_combs if sc not in s and not s.add(sc)]

        sample_pair_keys=[]
        M_keys = []
        skip_keys = []
        wpairs = []
        comb_types = []
        for pi,(i,j) in enumerate(pair_combs):
            m1,m2 = maps[i1],maps[i2]
            comb_types.append(pair_types_dict[(m1.spin,m2.spin)])
            sample_pair_key = (sample_map_nums[i], sample_map_nums[j])
            sample_pair_keys.append(sample_pair_key)
            M_key = (sample_combs[pi], sample_pair_key)
            M_keys.append(M_key)
            if weight_map_keys:
                wpair = (weight_map_keys[i],weight_map_keys[j])
                if wpair in wpairs:
                    skip_key = wpairs.index(wpair)
                else:
                    skip_key = None
                wpairs.append(wpair)
                skip_keys.append(skip_key)

        if verbose:
            print('pair_combs',pair_combs)
            print('samples',samples)
            print("sample_combs:",sample_combs)
            print("unique_sample_combs",unique_sample_combs)
            print("sample_map_nums",sample_map_nums)            

        #prepare dictionaries for output
        Ms = {}
        Cls_raw = OrderedDict()
        Cls_corrected = OrderedDict()
        Cls_noise = OrderedDict()
        Cls_noise_var = OrderedDict()
        M_arrays = {} #dict of binned mixing matrices
        Ms_cached = {}
        for sc in unique_sample_combs:
            Ms[sc] = {}
            Cls_raw[sc] = OrderedDict()
            Cls_corrected[sc] = OrderedDict()
            Cls_noise[sc] = OrderedDict()
            Cls_noise_var[sc] = {}
            M_arrays[sc] = {}

    else:
        Ms = {}
        skip_keys = None
        pair_combs = None
        field_types = None

    if comm:
        if rank==0:
            print('bcasting noise maps')
        skip_keys = comm.bcast(skip_keys, root=0)
        pair_combs = comm.bcast(pair_combs,root=0)
        field_types = comm.bcast(field_types, root=0)
        if rank==0:
            print('bcasting pair_combs and noise_maps: DONE')

    #now get the mixing matrices
    if not binned_Ms:
        Ms_list = get_mixing(clbinner, weight_maps, field_types, pair_combs=pair_combs, skip_keys=skip_keys, comm=comm, clip=clip)
        if rank==0:
            #now populate the Ms dictionary
            for pi in range(len(pair_combs)):
                sample_comb, sample_pair_key  = sample_combs[pi], sample_pair_keys[pi]
                Ms[sample_comb][sample_pair_key] = Ms_list[pi]
    else:
        Ms=binned_Ms

    #and now broadcast the mxinig matrices to all processes
    if comm:
        comm.bcast(Ms, root=0)

    if rank==0:
        #now  the Cls
        for pi,(i1,i2) in enumerate(pair_combs):
            sample_pair_key = sample_pair_keys[pi]
            sample_comb = sample_combs[pi]
            print("############################")
            print("rank %d computing Cls for sample combination (%s,%s),"%(rank, sample_comb[0],sample_comb[1]))
            print("bin combination (%d,%d)"%(sample_map_nums[i1],sample_map_nums[i2]))
            print("############################")

            m1,m2 = maps[i1],maps[i2]
            assert m1.nside==m2.nside
            comb_type = pair_types_dict[(m1.spin,m2.spin)]

            #Compute raw Cls
            pol = (comb_type=='TP' or comb_type=='PP')
            _cls_raw = hp.sphtfunc.anafast(m1.get_healpy_input(pol=pol), m2.get_healpy_input(pol=pol), pol = pol, lmax=clbinner.lmax)
            if comb_type=='TT':
                cls_raw = {}
                cls_raw['TT'] = _cls_raw
            elif comb_type=='TP':
                cls_raw_all = healpy_list_to_dict(_cls_raw)
                cls_raw = {"TE":cls_raw_all["TE"],"TB":cls_raw_all["TB"]}
            else:
                cls_raw_all = healpy_list_to_dict(_cls_raw)
                cls_raw = {"EE":cls_raw_all["EE"],"BB":cls_raw_all["BB"],"EB":cls_raw_all["EB"]}

            #Get corrected Cls
            M = Ms[sample_comb][sample_pair_key]
            cls_corrected = M.binned_inversion(cls_raw)
            Cls_raw[sample_comb][sample_pair_key] = cls_raw
            Cls_corrected[sample_comb][sample_pair_key] = cls_corrected

        print('Done cls')

    #if noise maps provided, assume we also want to do noise realisations 
    if noise_maps and noise_reals:
        if rank==0:
            print('doing noise realisations')
        for pi,(i1,i2) in enumerate(pair_combs):
            sample_pair_key = sample_pair_keys[pi]
            if i1==i2:
                if rank==0:
                    print('pair %d,%d'%(i1,i2))
                if noise_maps[i1] is not None:
                    res = noise_maps[i1].get_noise_cls(clbinner.lmax, noise_reals, comm=comm)
                    if rank==0:
                        noise_cls, noise_vars = res
                        Cls_noise[sample_comb][sample_pair_key[0]] = M.binned_inversion(noise_cls)
            else:
                if rank==0:
                    Cls_noise[sample_comb][sample_pair_key[0]] = zero_cl_dict(cls_corrected)
    if rank==0:
        print('Cls_noise',Cls_noise)
    if rank!=0:
        return noise_maps
    if rank==0:
        if ret_2pt_specs:
            import twopoint
            specs,noise_specs=[],[]
            if verbose:
                print('making twopoint spec objects')
                print('unique samples:',unique_sample_combs)
            for sc in unique_sample_combs:
                print('making spec for sample comb', sc)
                n_data = len(Cls_corrected[sc]) * clbinner.num_ell_bins
                #spec_type = spec_types[sc]
                #spec_name = spec_names[sc]
                if sc not in spec_info:
                    continue
                spec_types = spec_info[sc].types
                spec_name = spec_info[sc].spec_name
                for mode, spec_type in zip(spec_info[sc].modes,spec_info[sc].types):
                    #set up arrays for input into 2pt spec object
                    cl = np.zeros(n_data)
                    noise = np.zeros(n_data)
                    zbins = np.zeros(n_data, dtype=(int,2))
                    ang_bin = np.zeros(n_data,dtype=int)
                    ang = np.zeros(n_data, dtype=(float,2))
                    k=0
                    for bin_pair in Cls_corrected[sc].keys():
                        #print Cls_corrected[sc][bin_pair]
                        print('bin_pair',bin_pair)
                        inds = np.arange(k*clbinner.num_ell_bins,(k+1)*clbinner.num_ell_bins)
                        cl[inds] = Cls_corrected[sc][bin_pair][mode]
                        zbins[inds] = bin_pair 
                        ang_bin[inds] = np.arange(clbinner.num_ell_bins)
                        ang[inds] = zip(clbinner.ell_bin_lims[:-1],clbinner.ell_bin_lims[1:])
                        if noise_maps and noise_reals:
                            if (sc[0]==sc[1] and bin_pair[0]==bin_pair[1]):
                                print(Cls_noise)
                                noise[inds] = Cls_noise[sc][bin_pair[0]][mode]
                        k+=1
                    types = (getattr(twopoint.Types,spec_type[0]),getattr(twopoint.Types,spec_type[1]))
                    if sub_noise:
                        cl -= noise
                    spec = twopoint.SpectrumMeasurement(spec_name+"_"+mode, zbins.T+1, types, ("nz_"+sc[0],"nz_"+sc[1]),
                                                        "CLBP", ang_bin, cl, angle=ang, metadata=None)
                    specs.append(spec)
                    if noise_maps and noise_reals:
                        noise_spec = twopoint.SpectrumMeasurement(spec_name+"_"+mode+"_noise", zbins.T+1, types, ("nz_"+sc[0],"nz_"+sc[1]),
                                                              "CLBP", ang_bin, noise, angle=ang, metadata=None)
                        noise_specs.append(noise_spec)
                    
            return Cls_raw, Cls_corrected, Cls_noise, specs, noise_specs, Ms

        else:
            return Cls_raw, Cls_corrected, Cls_noise, Ms


def get_corrected_noise_cls(clbinner, noise_maps, binned_Ms, samples=None, noise_reals=100, spec_info=DEFAULT_SPEC_INFO,
                            comm=None, ret_2pt_spec=False, verbose=True):

    if comm:
        rank = comm.Get_rank()
        size = comm.Get_size()
        from mpi4py import MPI
        from mpi4py.MPI import ANY_SOURCE
    else:
        rank=0
        size=1

    field_types=[]
    pair_types_dict={(0,0):'TT', (0,2):'TP', (2,2):'PP'}
    num_maps = len(noise_maps)

    for i,m in enumerate(noise_maps):
        field_types.append(m.spin)

    print('field_types:',field_types)
    if samples is None:
        samples=[]
        for f in field_types:
            samples.append("lens" if f==0 else "source")

    #Get map combinations
    pair_combs=[]
    for i1 in range(num_maps):
        for i2 in range(i1,num_maps):
            pair_combs.append((i1,i2))
    sample_combs = [(samples[i],samples[j]) for (i,j) in pair_combs]
    s=set()
    unique_sample_combs = [sc for sc in sample_combs if sc not in s and not s.add(sc)]
    Cls_noise=OrderedDict()
    for u in unique_sample_combs:
        Cls_noise[u]=OrderedDict()

    #Get bin number for each map
    sample_map_nums = []
    samples_counted = []
    for s in samples:
        sample_map_nums.append(samples_counted.count(s))
        samples_counted.append(s)


    for pi,(i1,i2) in enumerate(pair_combs):
        sample_pair_key = (sample_map_nums[i1],sample_map_nums[i2])
        sample_comb = sample_combs[pi]
        if sample_comb[0]!=sample_comb[1]:
            continue
        comb_type = pair_types_dict[(field_types[i1],field_types[i2])]
        M = binned_Ms[sample_comb][sample_pair_key]
        if i1==i2:
            res = noise_maps[i1].get_noise_cls(clbinner.lmax, noise_reals, comm=comm)
            if rank==0:
                noise_cls, noise_vars = res
                Cls_noise[sample_comb][sample_pair_key] = M.binned_inversion(noise_cls)
        else:
            if rank==0:
                if noise_maps[i1].spin!=noise_maps[i2].spin:
                    continue
                else:
                    if noise_maps[i1].spin==2:
                        Cls_noise[sample_comb][sample_pair_key] = {"EE": np.zeros(clbinner.num_ell_bins),
                                                                   "BB": np.zeros(clbinner.num_ell_bins),
                                                                   "EB": np.zeros(clbinner.num_ell_bins)}
                    else:
                        Cls_noise[sample_comb][sample_pair_key] = {"TT": np.zeros(clbinner.num_ell_bins)}

    if ret_2pt_spec:
        import twopoint
        noise_specs=[]
        if verbose:
            print('making twopoint spec objects')
            print('unique samples:',unique_sample_combs)
        for sc in unique_sample_combs:
            print('making spec for sample comb', sc)
            n_data = len(Cls_noise[sc]) * clbinner.num_ell_bins
            if sc not in spec_info:
                continue
            spec_types = spec_info[sc].types
            spec_name = spec_info[sc].spec_name
            for mode, spec_type in zip(spec_info[sc].modes,spec_info[sc].types):
                #set up arrays for input into 2pt spec object
                noise = np.zeros(n_data)
                zbins = np.zeros(n_data, dtype=(int,2))
                ang_bin = np.zeros(n_data,dtype=int)
                ang = np.zeros(n_data, dtype=(float,2))
                k=0
                for bin_pair in Cls_noise[sc].keys():
                    inds = np.arange(k*clbinner.num_ell_bins,(k+1)*clbinner.num_ell_bins)
                    noise[inds] = Cls_noise[sc][bin_pair][mode]
                    zbins[inds] = bin_pair 
                    ang_bin[inds] = np.arange(clbinner.num_ell_bins)
                    ang[inds] = zip(clbinner.ell_bin_lims[:-1],clbinner.ell_bin_lims[1:])
                    k+=1
                types = (getattr(twopoint.Types,spec_type[0]),getattr(twopoint.Types,spec_type[1]))
                noise_spec = twopoint.SpectrumMeasurement(spec_name+"_"+mode, zbins.T+1, types, ("nz_"+sc[0],"nz_"+sc[1]),
                                                        "CLBP", ang_bin, noise, angle=ang, metadata=None)
                noise_specs.append(noise_spec)
                    
            return Cls_noise, noise_specs
        else:
            return Cls_noise
    

def wigglyCl(lmin,lmax, power=-2, wiggle_amp=0.2, wiggle_freq=5):
    #Generate a wiggly input Cl for testing
    ells = np.arange(lmin,lmax+1)
    c_ell = (1 + wiggle_amp*np.sin(2*np.pi*wiggle_freq*ells.astype(float)/lmax)) * (ells.astype(float))**power
    c_ell[~np.isfinite(c_ell)] = 0.
    c_ell[ells<2]=0.
    return ells, c_ell
